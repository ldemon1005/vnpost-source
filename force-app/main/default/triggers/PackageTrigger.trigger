trigger PackageTrigger on Package__c (before insert, after insert, before update) {
    if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isBefore){
        PackageTriggerHandler.updateBatch(Trigger.New);
        PackageTriggerHandler.updateOrder(Trigger.New);
        //PackageTriggerHandler.updateSaleOrder(Trigger.New);
    }
    
    //if(Trigger.isUpdate &&  Trigger.isBefore){
        //PackageTriggerHandler.updateSalesOrderUUID(Trigger.New);
    //}
    
    if(Trigger.isInsert && Trigger.isAfter){
        PackageTriggerHandler.updateItemPackage(Trigger.new);
        PackageTriggerHandler.updateReceiptPackage(Trigger.new);
        //PackageTriggerHandler.updateAddedValuePackage(Trigger.new);
        //PackageTriggerHandler.updateStatusPackage(Trigger.new);
    }
}