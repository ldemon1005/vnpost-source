trigger AutoShipmentFromCase on Case (before insert) {
    List<string> packageTexts = new List<string>();
    for(Case c : Trigger.New) {
        if (c.Package_text__c != '' && c.Package_text__c != null) {
            packageTexts.add(c.Package_text__c);
        }
    }
    List<Package__c> existPackages = [SELECT Id, Name, Package_Number__c FROM Package__c WHERE Name IN :packageTexts];
    List<Package__c> packagesToAdd = new List<Package__c>();
    for(Case c : Trigger.New) {
        if (c.Package_text__c != '' && c.Package_text__c != null) {
            Package__c existPackage = null;
            for (Package__c pk : existPackages) {
                if (pk.Name == c.Package_text__c) {
                    existPackage = pk;
                    break;
                }
            }
            if (existPackage != null) {
                continue;
            } 
            Package__c newPackage = new Package__c(Name = c.Package_text__c, Package_Number__c = c.Package_text__c);
            packagesToAdd.add(newPackage);
            existPackages.add(newPackage);
        }
    }
    insert packagesToAdd;
    for(Case c : Trigger.New) {
        if (c.Package_text__c != '' && c.Package_text__c != null) {
            Package__c existPackage = null;
            for (Package__c pk : existPackages) {
                if (pk.Name == c.Package_text__c) {
                    existPackage = pk;
                    break;
                }
            }
            if (existPackage == null) {
                continue;
            } 
            c.Package__c = existPackage.Id;
        }
    }
}