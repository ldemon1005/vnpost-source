trigger LeadTrigger on Lead (before insert) {
	if(Trigger.isInsert) {
        if(Trigger.isBefore){
            //tao ScrmId__c
            GuidGenerator.genSuiteCrmId(Trigger.new);
        }
        
        // after insert call out create lead
    }
}