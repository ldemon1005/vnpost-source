trigger ContractUploadTrigger2 on ContentDocumentLink (after insert, after delete) {
    System.debug('ContractUploadTrigger2');
	List<Id> listContractIds = new List<Id>();
    for(ContentDocumentLink attachment: Trigger.New) {
        System.debug(attachment);
        Schema.sObjectType sObjectType = attachment.LinkedEntityId.getSObjectType();
        System.debug(sObjectType);
        if ( sObjectType == Contract.sObjectType )
        {
            System.debug('sObjectType == Contract.sObjectType');
            listContractIds.add(attachment.LinkedEntityId);
            continue;
        }
    }
    List<ContentDocumentLink> attachmentWithContractIdsHasFileUploaded = [SELECT LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :listContractIds];
    List<Id> contractIdsHasFileUploaded = new List<Id>();
    for(ContentDocumentLink attachment: attachmentWithContractIdsHasFileUploaded) {
        contractIdsHasFileUploaded.add(attachment.LinkedEntityId);
    }
    
    List<Contract> contractsToUpdate = new List<Contract>();
    List<Contract> contractsHasFileUploaded = [SELECT ID, Has_File_Uploaded__c FROM Contract WHERE ID IN :contractIdsHasFileUploaded];
    for(Contract contract: contractsHasFileUploaded) {
        contract.Has_File_Uploaded__c = true;
        contractsToUpdate.add(contract);
    }
    System.debug(contractsHasFileUploaded);
    List<Contract> contractsHasNoFileUploaded = [SELECT ID, Has_File_Uploaded__c FROM Contract WHERE ID NOT IN :contractIdsHasFileUploaded AND ID IN :listContractIds];
    for(Contract contract: contractsHasNoFileUploaded) {
        contract.Has_File_Uploaded__c = false;
        contractsToUpdate.add(contract);
    }
    System.debug(contractsHasNoFileUploaded);
    update contractsToUpdate;
}