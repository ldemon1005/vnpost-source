trigger TicketHoTroTrigger on Ho_tro__c (before insert, before update, after insert, after update) {
    if(Trigger.isInsert && Trigger.isBefore){
        TicketHoTroHandler.updateCaseNumber(Trigger.New);
        GuidGenerator.generateUUIDSObject(Trigger.new);
    }
}