trigger AutoContactFromCase on Case (before insert) {
    for(Case c : Trigger.New) {
        if (c.SuppliedName != '' && c.SuppliedName != null) {
            List<Contact> existContacts = [SELECT Id, LastName, Email FROM Contact WHERE Phone != NULL AND Phone != '' AND Phone = :c.SuppliedPhone LIMIT 1];
            if (existContacts.isEmpty()) {
                existContacts = [SELECT Id, LastName, Email FROM Contact WHERE Email != NULL AND Email != '' AND Email = :c.SuppliedEmail LIMIT 1];
            }
            if (existContacts.isEmpty()) {
                Contact contact = new Contact(LastName = c.SuppliedName, Email = c.SuppliedEmail, Phone = c.SuppliedPhone);
                insert contact;
                c.ContactId = contact.Id;
            } 
            else {
                Contact contact = existContacts[0];
                if (c.SuppliedEmail != null && c.SuppliedEmail != '' && contact.Email != c.SuppliedEmail) {
                    contact.Email = c.SuppliedEmail;
                    upsert contact;
                }
                c.ContactId = contact.Id;
            }
        }
    }
}