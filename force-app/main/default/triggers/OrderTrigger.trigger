trigger OrderTrigger on Order (before insert, before update, after insert, after update) {
    if(Trigger.isInsert && Trigger.isAfter) {
        SalesOrderTriggerHandler.updateBatch(Trigger.new);
        SalesOrderTriggerHandler.updatePackage(Trigger.new);
    }  
}