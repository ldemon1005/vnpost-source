trigger ContractUploadTrigger3 on ContentDocument (before delete) {
	System.debug('ContractUploadTrigger3');
    List<Id> contentDocId = new List<Id>();
    for(ContentDocument con : Trigger.old){
        contentDocId.add(con.Id);
    }
    System.debug('contentDocId');
    System.debug(contentDocId);
    List<Id> listContractIds = new List<Id>();
    List<ContentDocumentLink> cdls = [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocId];
    System.debug('cdls');
    System.debug(cdls);
    for(ContentDocumentLink attachment: cdls) {
        System.debug(attachment);
        Schema.sObjectType sObjectType = attachment.LinkedEntityId.getSObjectType();
        System.debug(sObjectType);
        if (sObjectType == Contract.sObjectType)
        {
            System.debug('sObjectType == Contract.sObjectType');
            listContractIds.add(attachment.LinkedEntityId);
            continue;
        }
    }
    List<String> contractIdsToUpdate = new List<String>();
    for (AggregateResult ar: 
         [SELECT LinkedEntityId, COUNT(Id) c
          FROM ContentDocumentLink WHERE LinkedEntityId IN :listContractIds 
          GROUP BY LinkedEntityId]) {
              String id = String.valueOf(ar.get('LinkedEntityId'));
              Integer count = Integer.valueOf(ar.get('c'));
              System.debug('ar');
              System.debug(ar);
              if (count <= 1) {
                  contractIdsToUpdate.add(id);
              }
    }
    System.debug('contractIdsToUpdate');
    System.debug(contractIdsToUpdate);
    
    List<Contract> contractsToUpdate = new List<Contract>();
    List<Contract> contractsHasNoFileUploaded = [SELECT ID, Has_File_Uploaded__c FROM Contract WHERE ID IN :contractIdsToUpdate];
    for(Contract contract: contractsHasNoFileUploaded) {
        contract.Has_File_Uploaded__c = false;
        contractsToUpdate.add(contract);
    }
    update contractsToUpdate;
}