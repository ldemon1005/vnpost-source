trigger StatusTrigger on Status__c (before insert, before update) {
    if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isBefore){
        Map<String, Status__c> mapStatusNew = StatusTriggerHandler.updatePackageLookup(Trigger.new);
        for(Status__c status : Trigger.new){
            if(status.From__c == 'MYVNPOST' || status.From__c == 'BCCP' || status.From__c == 'GATEWAY'){
                if(mapStatusNew.get(status.id) != null){
                    status = mapStatusNew.get(status.id);
                }
            }
        }
    }
    
    if(Trigger.isUpdate && Trigger.isBefore){
        Map<String, Status__c> mapStatusNew = StatusTriggerHandler.updatePackageUUID(Trigger.new);
        for(Status__c status : Trigger.new){
            if(Trigger.oldMap.get(status.Id).Tracking_Code__c != Trigger.newMap.get(status.Id).Tracking_Code__c
               && (status.From__c == 'MYVNPOST' || status.From__c == 'BCCP' || status.From__c == 'GATEWAY')){
                if(mapStatusNew.get(status.id) != null){
                    status = mapStatusNew.get(status.id);
                }
            }
        }
    }
}