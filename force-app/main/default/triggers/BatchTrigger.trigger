trigger BatchTrigger on Batch__c (before insert, after insert, before update) {
    if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isBefore){
        BatchTriggerHandler.updateOrder(Trigger.New);
    }
    
    if(Trigger.isUpdate && Trigger.isBefore){
        BatchTriggerHandler.updateSaleOrderUUID(Trigger.New);
    }
    
    if(Trigger.isInsert && Trigger.isAfter){
        BatchTriggerHandler.updateBatchOnPackage(Trigger.New);
    }
    
}