trigger LeadCreateTaskFromCase on Lead (after insert) {
   for(Lead leadObj : Trigger.new){
            if(String.isNotBlank(leadObj.Longitude__c) && String.isNotBlank(leadObj.Latitude__c) ){
                Case caseObj = [Select Id, Subject From Case where Id =: leadObj.Latitude__c limit 1];
                if(caseObj != null) {
                    Task taskItem = new Task();
                    Id leadIdStr = leadObj.Id;
                    Id caseIdStr = caseObj.Id;
                    taskItem.Subject = 'Lead: ' + leadObj.LastName +  ' tạo từ Case: ' + leadObj.Longitude__c;
                    taskItem.WhoId = leadIdStr;
                    taskItem.Case__c = caseIdStr;
                    insert taskItem;
                    
                 Lead leadUpdate = [Select Id, Name from Lead where Id =: leadObj.Id limit 1];
                    if(leadUpdate != null) {
                        leadUpdate.Longitude__c = '';
                    	leadUpdate.Latitude__c = '';
                    	update leadUpdate;
                    }
                    
                }
            }
        }
}