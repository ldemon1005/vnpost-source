trigger ItemTrigger on Item__c (before insert, before update) {
    if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isBefore){
        Map<String, Item__c> mapItemNew = ItemTriggerHandler.updatePackageLookup(Trigger.new);
        for(Item__c item : Trigger.new){
            if(item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY'){
                if(mapItemNew.get(item.id) != null){
                    item = mapItemNew.get(item.id);
                }
            }
        }
    }
    
    if(Trigger.isUpdate && Trigger.isBefore){
        Map<String, Item__c> mapItemNew = ItemTriggerHandler.updatePackageUUID(Trigger.new);
        for(Item__c item : Trigger.new){
            if(Trigger.oldMap.get(item.Id).Package__c != Trigger.newMap.get(item.Id).Package__c
               && (item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY')){
                if(mapItemNew.get(item.id) != null){
                    item = mapItemNew.get(item.id);
                }
            }
        }
    }
}