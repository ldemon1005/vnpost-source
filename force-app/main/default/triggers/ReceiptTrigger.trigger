trigger ReceiptTrigger on Receipt__c (before insert, before update) {
    if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isBefore){
        Map<String, Receipt__c> mapReceiptNew = ReceiptTriggerHandler.updatePackageLookup(Trigger.new);
        for(Receipt__c receipt : Trigger.new){
            if(mapReceiptNew.get(receipt.id) != null){
                receipt = mapReceiptNew.get(receipt.id);
            }
        }
    }
    
    if(Trigger.isUpdate && Trigger.isBefore){
        Map<String, Receipt__c> mapReceiptNew = ReceiptTriggerHandler.updatePackageUUID(Trigger.new);
        for(Receipt__c receipt : Trigger.new){
            if(Trigger.oldMap.get(receipt.Id).Package__c != Trigger.newMap.get(receipt.Id).Package__c){
                if(mapReceiptNew.get(receipt.id) != null){
                    receipt = mapReceiptNew.get(receipt.id);
                }
            }
        }
    }
}