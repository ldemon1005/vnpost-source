trigger ContractUploadTrigger on Attachment (after insert, after delete) {
	List<Id> listContractIds = new List<Id>();
    for(Attachment attachment: Trigger.New) {
        System.debug(attachment);
        Schema.sObjectType sObjectType = attachment.ParentId.getSObjectType();
        if ( sObjectType == Contract.sObjectType )
        {
            listContractIds.add(attachment.ParentId);
            continue;
        }
    }
    List<Attachment> attachmentWithContractIdsHasFileUploaded = [SELECT ParentId FROM Attachment WHERE ParentId IN :listContractIds];
    List<Id> contractIdsHasFileUploaded = new List<Id>();
    for(Attachment attachment: attachmentWithContractIdsHasFileUploaded) {
        contractIdsHasFileUploaded.add(attachment.ParentId);
    }
    List<Contract> contractsToUpdate = new List<Contract>();
    List<Contract> contractsHasFileUploaded = [SELECT ID, Has_File_Uploaded__c FROM Contract WHERE ID IN :contractIdsHasFileUploaded];
    for(Contract contract: contractsHasFileUploaded) {
        contract.Has_File_Uploaded__c = true;
        contractsToUpdate.add(contract);
    }
    List<Contract> contractsHasNoFileUploaded = [SELECT ID, Has_File_Uploaded__c FROM Contract WHERE ID NOT IN :contractIdsHasFileUploaded AND ID IN :listContractIds];
    for(Contract contract: contractsHasFileUploaded) {
        contract.Has_File_Uploaded__c = false;
        contractsToUpdate.add(contract);
    }
    update contractsToUpdate;
}