trigger CaseTrigger on Case (before insert, before update, after insert, after update) {
    if(Trigger.isInsert) {
        if(Trigger.isBefore){
            //khách vãng lai
            list<Account> l_account = [select id from Account where Ma_khach_hang__c = '99999999999999999' FOR UPDATE];
            if(l_account.size() > 0) Account acc = l_account[0];
            for(Case c : Trigger.new){
               
                if(String.isEmpty(c.AccountId) && l_account.size() > 0){
                    c.AccountId = l_account[0].Id;
                }
            }
            CaseTriggerHandler.checkDuplicatedCase(Trigger.new);
        	//
            CaseTriggerHandler.updateFacebookType(Trigger.new);
            //tao uuid
            GuidGenerator.generateUUIDSObject(Trigger.new);
        }
        
        if(Trigger.isAfter){
            User u = [select id,Employee_Online__c from User where id =: userInfo.getUserId()];
       		List<Case_Employee__c> l_case_employee = new List<Case_Employee__c>();
            for(Case c : Trigger.new){
                Case_Employee__c case_employee = new Case_Employee__c();
                case_employee.Case__c = c.Id;
                case_employee.Employee__c = u.Employee_Online__c;
                case_employee.Action__c = 'insert case';
                l_case_employee.add(case_employee);
            }
            if(l_case_employee.size() > 0){
                System.debug(l_case_employee);
            	insert l_case_employee;    
            }
            CaseTriggerHandler.updateTicketHoTro(Trigger.new);
        }
    }
    
    if(Trigger.isUpdate) {
        if(Trigger.isBefore) {
            for(String cId : Trigger.newMap.keySet()) {
                 if((Trigger.oldMap.get(cId).Package__c != Trigger.newMap.get(cId).Package__c)
                 || (Trigger.oldMap.get(cId).Li_do_khieu_nai__c != Trigger.newMap.get(cId).Li_do_khieu_nai__c)
                 || (Trigger.oldMap.get(cId).Case_Reference_Code__c != Trigger.newMap.get(cId).Case_Reference_Code__c)) {
                     CaseTriggerHandler.checkDuplicatedCase(Trigger.new);
                 }
            }
            CaseTriggerHandler.updateFacebookType(Trigger.new);
        }
        
        if(Trigger.isAfter){
            User u = [select id,Employee_Online__c from User where id =: userInfo.getUserId()];
       		List<Case_Employee__c> l_case_employee = new List<Case_Employee__c>();
            for(Case c : Trigger.new){
                Case_Employee__c case_employee = new Case_Employee__c();
                case_employee.Case__c = c.Id;
                case_employee.Employee__c = u.Employee_Online__c;
                case_employee.Action__c = 'update case';
                l_case_employee.add(case_employee);
            }
            if(l_case_employee.size() > 0){
            	insert l_case_employee;    
            }
        }
    }
}