trigger TaskTrigger on Task (before insert, after insert) {
    if(Trigger.isBefore){
        User u = [select id,Employee_Online__c from User where id =: userInfo.getUserId()];
        for(Task c : Trigger.new){
      		c.Employee__c = u.Employee_Online__c;
        }
    }
}