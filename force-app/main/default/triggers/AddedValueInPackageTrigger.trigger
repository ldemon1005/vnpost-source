trigger AddedValueInPackageTrigger on Added_Value_in_Package__c (before insert, before update) {
    if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isBefore){
        Map<String, Added_Value_in_Package__c> mapAdded_Value_in_PackageNew = AddedValueInPackageTriggerHandler.updatePackageLookup(Trigger.new);
        for(Added_Value_in_Package__c addedValue : Trigger.new){
            if(addedValue.From__c == 'MYVNPOST' || addedValue.From__c == 'BCCP' || addedValue.From__c == 'GATEWAY'){
                if(mapAdded_Value_in_PackageNew.get(addedValue.id) != null){
                    addedValue = mapAdded_Value_in_PackageNew.get(addedValue.id);
                }
            }
        }
    }
    
    if(Trigger.isUpdate && Trigger.isBefore){
        Map<String, Added_Value_in_Package__c> mapAddedValueNew = AddedValueInPackageTriggerHandler.updatePackageUUID(Trigger.new);
        for(Added_Value_in_Package__c addedValue : Trigger.new){
            if(Trigger.oldMap.get(addedValue.Id).Tracking_Code__c != Trigger.newMap.get(addedValue.Id).Tracking_Code__c && (addedValue.From__c == 'MYVNPOST' || addedValue.From__c == 'BCCP' || addedValue.From__c == 'GATEWAY')){
                if(mapAddedValueNew.get(addedValue.id) != null){
                    addedValue = mapAddedValueNew.get(addedValue.id);
                }
            }
        }
    }
}