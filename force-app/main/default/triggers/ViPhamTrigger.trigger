trigger ViPhamTrigger on Vi_pham__c (before insert) {
    if(Trigger.isInsert&Trigger.isBefore){
        GuidGenerator.generateUUIDSObject(Trigger.new);
    }
}