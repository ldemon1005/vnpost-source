public with sharing class ChatBOT{
    public class OutPut{
        @InvocableVariable(required=true)
        public String sStatus;
    }
    public class InPut{
        @InvocableVariable(required=true)
        public String input;
    }
    @InvocableMethod(label='get Status')
    public static List<OutPut> getOrderStatus(List<String> orderInputs) {
        List<OutPut> orderOutputs = new List<OutPut>();
        OutPut orderOutput = new OutPut();
        List <Status__c> items = [SELECT StatusText__c, Datetime_Created__c,Location__c FROM Status__c WHERE Tracking_Code__c in (select Id from Package__c where Name=:orderInputs.get(0)) 
                                  ORDER BY Datetime_Created__c DESC NULLS LAST limit 1];
        if(items.size()>0){
            orderOutput.sStatus= 'Đơn hàng '+orderInputs[0]+' '+items.get(0).StatusText__c + ' - '+ items.get(0).Datetime_Created__c+ ' - '+ items.get(0).Location__c;
        } else {
            orderOutput.sStatus= 'Đơn hàng '+orderInputs[0]+' chưa có trạng thái';
            
            orderOutputs.add(orderOutput );   
        }
        return orderOutputs;
    }
}