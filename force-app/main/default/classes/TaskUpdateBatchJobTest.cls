@isTest
public class TaskUpdateBatchJobTest {
    static testMethod void testMethod1() 
    {
        List<Task> tasks = new List<Task>();
        
        for(Integer i=0 ;i <3;i++)
        {
            Task t = new Task();
            t.Subject = 'Subject'+i;
            t.Call_Id__c = '902323';
            t.Accepted__c = 'accepted';
            if(i == 1){
            	t.Refer_To__c = '0356023999';
            }
            // t.Record_Url__c = 'http://';
            tasks.add(t);
        }
        insert tasks;
        
        Test.startTest();
        if(!tasks.isEmpty()){
            List<String> myCids = new List<String>();
            for(Task t : tasks){
                if(t.Call_Id__c != null){
                    myCids.add(t.Call_Id__c);
                }
            }
            if(myCids != null){
                Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
                // Call method to test
                HTTPResponse response = CalloutCallCenter.logs(myCids);
                Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                List<Object> tasksRes = (List<Object>)m.get('body');
                if(tasksRes.size() > 0){
                    for(Task t : tasks ){
                        for(Object ts : tasksRes){
                            Map<String,Object> mts = (Map<string,Object>)ts;
                            if(t.get('Refer_To__c') != null){
                                if((t.Call_Id__c == mts.get('call_id') || t.Call_Id__c == mts.get('callee_id')) && t.Refer_To__c == mts.get('refer')){
                                    t.Record_Url__c = (String) mts.get('record_url');
                                    t.CallDurationInSeconds = (Integer) mts.get('duration');
                                }
                            }else{
                                if((t.Call_Id__c == mts.get('call_id') || t.Call_Id__c == mts.get('callee_id')) && (mts.get('refer') == null || mts.get('refer') == '')){
                                    t.Record_Url__c = (String) mts.get('record_url');
                                    t.CallDurationInSeconds = (Integer) mts.get('duration');
                                }
                            }
                        }
                    }    
                }else{
                    //return null;
                }
            }else{
                //return null;
            }
        }
        
        UpdateTask obj = new UpdateTask();
        String jobId = System.schedule('ScheduledApexTest', '0 0 * * * ?', obj);
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
}