public class PackageTriggerHandler {
    /*public static void preventDuplicate( List<Package__c> listPackage){
        List<String> packageNumber = new List<String>();
        for(Package__c pack : listPackage){
            packageNumber.add(pack.Package_Number__c);
        }
        System.debug(packageNumber);
		List<package__c> l_package = [select id, Package_Number__c from package__c where package_number__c in : packageNumber];
        System.debug('hello');
        System.debug(l_package);
        if(l_package.size() > 0){
            for(Package__c p : l_package){
                for(Package__c pa : listPackage){
                    if(p.Package_Number__c == pa.Package_Number__c){
                        pa.addError('duplicate package number: ' + pa.Package_Number__c);
                    }
                }
            }
        }
    }*/
    public static void updateBatch( List<Package__c> listPackage){
        List<String> listBatchNumber = new List<String>();
        List<Package__c> listPackageUpdate = new List<Package__c>();
        //Map<String,String> mapMerchant = new Map<String,String>();
        for(Package__c p : listPackage){
            if(p.Batch_Number__c!= null &&(p.From__c=='MYVNPOST' || p.From__c =='BCCP' || p.From__c =='GATEWAY'))
            {
                listBatchNumber.add(p.Batch_Number__c);
                listPackageUpdate.add(p);
                //mapMerchant.put(p.SalesOrder_Number__c, p.From__c);
            }
        }
        
        List<Batch__c> l_batch = [Select Id,Batch_Number__c FROM Batch__c Where Batch_Number__c IN :listBatchNumber 
                         AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
        
        for(Batch__c b : l_batch)
        {
            for(Package__c p : listPackageUpdate){
                if(p.Batch_Number__c == b.Batch_Number__c){
                    p.Batch__c = b.Id;
                    p.IsUpdated__c = true;
                }
            }
        }
    }
    /*public static void updateSaleOrder( List<Package__c> listPackage){
        List<String> listUUID  = new List<String>();
        List<Package__c> listPackageUpdate = new List<Package__c>();
        //Map<String,String> mapMerchant = new Map<String,String>();
        for(Package__c p : listPackage){
            if(p.SalesOrder_Number__c != null &&(p.From__c=='MYVNPOST' || p.From__c =='BCCP' || p.From__c =='GATEWAY')){
                listUUID.add(p.SalesOrder_Number__c );
                listPackageUpdate.add(p);
                //mapMerchant.put(p.SalesOrder_Number__c, p.From__c);
            }
        }
        
        List<Order> l_salesOrder = [Select Id,SalesOrderNumber__c  FROM Order Where SalesOrderNumber__c  IN :listUUID 
                                      AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
        
        for(Order s : l_salesOrder)
        {
            for(Package__c p : listPackageUpdate){
                if(p.SalesOrder_Number__c  == s.SalesOrderNumber__c ){
                    p.Order_Number__c = s.Id;
                    p.IsUpdated__c = true;
                }
            }
        }
    }*/
    
    public static void updateItemPackage( List<Package__c> listPackage){
        Map<String, String> l_package = new Map<String, String>();
        for(Package__c pa : listPackage){
            if(pa.From__c =='MYVNPOST' || pa.From__c  =='BCCP' || pa.From__c == 'GATEWAY'){
                l_package.put(pa.Package_Number__c, pa.Id);
            }
        }
        list<item__c> l_item = [select id, package__c, Package_Number__c from item__c where Package_Number__c in : l_package.keySet() 
                                AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
       
        if(l_item.size() > 0){
            for(Item__c item : l_item){
                if(String.isEmpty(item.package__c)){
                    item.package__c = l_package.get(item.Package_Number__c);
                }
            }
            update l_item;
        }
    }
    
    public static void updateReceiptPackage( List<Package__c> listPackage){
        Map<String, String> l_package = new Map<String, String>();
        for(Package__c pa : listPackage){
            if(pa.From__c =='MYVNPOST' || pa.From__c  =='BCCP' || pa.From__c == 'GATEWAY'){
                l_package.put(pa.Package_Number__c, pa.Id);
            }
        }
        list<Receipt__c> l_receipt = [select id, package__c, Package_Number__c from Receipt__c where Package_Number__c in : l_package.keySet() 
                                AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
       
        if(l_receipt.size() > 0){
            for(Receipt__c item : l_receipt){
                if(String.isEmpty(item.Package_Number__c)){
                    item.Package_Number__c = l_package.get(item.Package_Number__c);
                }
            }
            update l_receipt;
        }
    }
    
    /*public static void updateAddedValuePackage( List<Package__c> listPackage){
        Map<String, String> l_package = new Map<String, String>();
        for(Package__c pa : listPackage){
            if(pa.From__c =='MYVNPOST' || pa.From__c  =='BCCP' || pa.From__c == 'GATEWAY'){
                l_package.put(pa.Package_Number__c, pa.Id);
            }
        }
        list<Added_Value_in_Package__c> l_addedValue = [select id,Package_Number__c,Tracking_Code__c  from Added_Value_in_Package__c where Package_Number__c in : l_package.keySet() 
                                      AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
       
        if(l_addedValue.size() > 0){
            for(Added_Value_in_Package__c item : l_addedValue){
                if(String.isEmpty(item.Package_Number__c)){
                    item.Package_Number__c = l_package.get(item.Package_Number__c);
                }
            }
            update l_addedValue;
            System.debug(l_addedValue);
        }
    }*/
    
    /*public static void updateStatusPackage( List<Package__c> listPackage){
        Map<String, String> l_package = new Map<String, String>();
        for(Package__c pa : listPackage){
            if(pa.From__c =='MYVNPOST' || pa.From__c  =='BCCP' || pa.From__c == 'GATEWAY'){
                l_package.put(pa.Package_Number__c, pa.Id);
            }
        }
        list<Status__c> l_status = [select id,Tracking_Code__c, Package_Number__c  from Status__c where Package_Number__c in : l_package.keySet() 
                                      AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
        
        if(l_status.size() > 0){
            for(Status__c item : l_status){
                if(String.isEmpty(item.Package_Number__c)){
                    item.Package_Number__c = l_package.get(item.Package_Number__c);
                }
            }
            update l_status;
        }
    }*/
    
    public static void updateOrder( List<Package__c> listPackage){
        List<String> listUUID  = new List<String>();
        List<Package__c> listPackageUpdate = new List<Package__c>();
        //Map<String,String> mapMerchant = new Map<String,String>();
        for(Package__c p : listPackage){
            if(p.SalesOrder_Number__c != null &&(p.From__c=='MYVNPOST' || p.From__c =='BCCP' || p.From__c =='GATEWAY')){
                listUUID.add(p.SalesOrder_Number__c );
                listPackageUpdate.add(p);
                //mapMerchant.put(p.SalesOrder_Number__c, p.From__c);
            }
        }
        
        List<Order> l_salesOrder = [Select Id,SalesOrderNumber__c  FROM Order Where SalesOrderNumber__c  IN :listUUID 
                                      AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
        
        for(Order s : l_salesOrder)
        {
            for(Package__c p : listPackageUpdate){
                if(p.SalesOrder_Number__c  == s.SalesOrderNumber__c ){
                    p.Order_Number__c = s.Id;
                    p.IsUpdated__c = true;
                }
            }
        }
    }
    
    /*public static void updateSalesOrderUUID( List<Package__c> listPackage){
        List<String> listSalesOrderId  = new List<String>();
        List<Package__c> listPackageUpdate = new List<Package__c>();
        for(Package__c p : listPackage){
            if(p.SalesOrder_Number__c != null &&(p.From__c=='MYVNPOST' || p.From__c =='BCCP' || p.From__c =='GATEWAY')){
                listSalesOrderId.add(p.SalesOrder_Number__c );
                listPackageUpdate.add(p);
            }
        }
        for(SalesOrder__c s :[Select Id,SalesOrder_Number__c  FROM SalesOrder__c Where id IN :listSalesOrderId 
                              AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')])
        {
            for(Package__c p : listPackageUpdate){
                if(p.SalesOrder__c == s.id){
                    p.SalesOrder_Number__c  = s.SalesOrder_Number__c ;
                }
            }
        }
    }*/
}