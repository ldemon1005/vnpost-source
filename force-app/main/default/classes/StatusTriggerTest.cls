@isTest
public class StatusTriggerTest {
    @isTest static void test(){
        Package__c pac1 = new Package__c();
        pac1.package_number__c = 'test2020';
        Package__c pac2 = new Package__c();
        pac2.package_number__c = 'test2021';
        
        List<Package__c> l_pac = new List<Package__c>();
        l_pac.add(pac1);
        l_pac.add(pac2);
        insert l_pac;
        
        Status__c status = new Status__c();
        status.From__c = 'MYVNPOST';
        status.package_number__c = 'test2020';
        insert status;
        
        status.Package_Number__c = '';
        status.Tracking_Code__c = l_pac[1].Id;
        update status;
    }
}