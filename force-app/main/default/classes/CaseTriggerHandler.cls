public class CaseTriggerHandler {
    public static void checkDuplicatedCase(List<Case> cList) {
		List<String> s = new list<String>();
        for(Case c : cList){
            if(!String.isEmpty(c.ContactId) && !String.isEmpty(c.Subject)) s.add(' (contact.id = \'' + c.ContactId + '\' and subject = \'' + c.Subject + '\') ');
        }
        String condition = String.join(s, ' or ');
        String soql_str;
        if(!String.isEmpty(condition)){
            soql_str = 'select id, contactId, subject from case where ' + condition;
            List<Case> l_case = Database.query(soql_str);
            System.debug(l_case);
            for(Case c1 : cList){
                if(c1.Origin == 'Web chat'){
                    for(Case c2 : l_case){
                        if(c1.ContactId == c2.ContactId && c1.subject == c2.subject) {
                            c1.addError('duplicate case by contact and subject');
                        }
                    }   
                }
            }
        }
    }
    public static void updateTicketHoTro(List<Case> listCase){
        List<String> listUUID = new List<String>();
        List<Case> caseMYVNPOST = new List<Case>();
        for(Case c :listCase){
            if((c.From__c =='MYVNPOST' || c.From__c == 'GATEWAY') && c.UUID__c !=NULL){
                listUUID.add(c.UUID__c);
                caseMYVNPOST.add(c);
            }
        }
        if(listUUID.size()>0){
            List<Ho_tro__c> listTickets = [Select Id,Case_Number__c,F_UUID__c From Ho_tro__c Where F_UUID__c IN :listUUID
                                           AND (From__c= 'MYVNPOST' OR From__c = 'GATEWAY') AND Case_Number__c =NUll];
            for (Ho_tro__c ticket : listTickets){
                for(Case c :caseMYVNPOST){
                    if(c.UUID__c == ticket.F_UUID__c){
                        ticket.Case_Number__c = c.Id;
                    }
                }                     
            }
            if(listTickets.size()>0){
                update listTickets;
            }
        }
    }
    
    public static void updateFacebookType(List<Case> l_case){
        System.debug('hello from cmc');
        System.debug(Schema.sObjectType.SocialPost.isAccessible());
        if(Schema.sObjectType.SocialPost.isAccessible()){
            for(Case c : l_case){
                System.debug(c.Source.name);
                if(c.Origin == 'Facebook' && c.Source.name != null){
                    System.debug(c.Source.name.startsWith('Inbox'));
                    if(c.Source.name.startsWith('Inbox')){
                        c.Facebook_type__c = 'Inbox';
                    }
                    if(c.Source.name.startsWith('Comment')){
                        c.Facebook_type__c = 'Comment';
                    }
                }
            }
        }
    }
}