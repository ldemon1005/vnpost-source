public class AddedValueInPackageTriggerHandler {
    public static Map<String,Added_Value_in_Package__c> updatePackageLookup(list<Added_Value_in_Package__c> l_addedValue){
        list<String> l_id = new list<String>();
        Map<String,Added_Value_in_Package__c> mapAdded_Value_in_PackageNew = new Map<String,Added_Value_in_Package__c>();
        Map<String,Added_Value_in_Package__c> mapAdded_Value_in_Package = new Map<String,Added_Value_in_Package__c>();
        Map<String,String> mapMerchant = new Map<String,String>();
        for(Added_Value_in_Package__c item : l_addedValue){
            if(item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY'){
                mapAdded_Value_in_Package.put(item.package_number__c , item);
                l_id.add(item.package_number__c);
                mapMerchant.put(item.package_number__c, item.From__c);
            }
        }
        list<Package__c> l_package = [select id, package_number__c  from Package__c where package_number__c  in : l_id];
        
        if(l_package.size() > 0){
            for(Package__c pa : l_package){
                Added_Value_in_Package__c addedValue = mapAdded_Value_in_Package.get(pa.package_number__c);
                if(addedValue != null){
                    addedValue.Tracking_Code__c = pa.id;
                    //addedValue.IsUpdated__c = true;
                    mapAdded_Value_in_PackageNew.put(addedValue.id,addedValue);
                }
            }
        }
        return mapAdded_Value_in_PackageNew;
    }
    
    public static Map<String,Added_Value_in_Package__c> updatePackageUUID(list<Added_Value_in_Package__c> l_addedValue){
        list<String> l_id = new list<String>();
        Map<String,Added_Value_in_Package__c> mapAdded_Value_in_PackageNew = new Map<String,Added_Value_in_Package__c>();
        Map<String,Added_Value_in_Package__c> mapAdded_Value_in_Package = new Map<String,Added_Value_in_Package__c>();
        for(Added_Value_in_Package__c item : l_addedValue){
            if(item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY'){
                mapAdded_Value_in_Package.put(item.Tracking_Code__c , item);
                l_id.add(item.Tracking_Code__c);
            }
        }
        list<Package__c> l_package = [select id, package_number__c  from Package__c where id  in : l_id];
        if(l_package.size() > 0){
            for(Package__c pa : l_package){
                Added_Value_in_Package__c addedValue = mapAdded_Value_in_Package.get(pa.package_number__c);
                if(addedValue != null){
                    addedValue.package_number__c = pa.package_number__c;
                    mapAdded_Value_in_PackageNew.put(addedValue.id,addedValue);
                }
            }
        }
        return mapAdded_Value_in_PackageNew;
    }
}