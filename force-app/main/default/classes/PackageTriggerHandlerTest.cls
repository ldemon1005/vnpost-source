@isTest
public class PackageTriggerHandlerTest {
    @isTest static void test(){
        Batch__c batch = new Batch__c();
        batch.From__c = 'MYVNPOST';
        batch.Batch_Number__c = 'test2020';
        batch.SalesOrder_Number__c = 'test2020';
        insert batch;
        
        Account acc = new Account(name = 'test2020');
        insert acc;
        
        Order o = new Order();
        o.SalesOrderNumber__c = 'test2020';
        o.AccountId = acc.id;
        o.EffectiveDate = date.today();
        o.Status = 'Draft';
        o.From__c = 'MYVNPOST';
        o.SalesOrderNumber__c = 'test2020';
        insert o;
        
        item__c item = new item__c();
        item.Package_Number__c = 'test2020';
        insert item;
        
        Receipt__c rec = new Receipt__c();
        rec.Package_Number__c = 'test2020';
        insert rec;
        
        Added_Value_in_Package__c added_value = new Added_Value_in_Package__c();
        added_value.Package_Number__c = 'test2020';
        insert added_value;
        
        Status__c status = new Status__c();
        status.Package_Number__c = 'test2020';
        insert status;
        
        Package__c pac1 = new Package__c();
        pac1.package_number__c = 'test2020';
        pac1.Batch_Number__c = 'test2020';
        pac1.SalesOrder_Number__c = 'test2020';
        pac1.From__c = 'MYVNPOST';
        pac1.Order_Number__c = 'test2020';
        Package__c pac2 = new Package__c();
        pac2.Batch_Number__c = 'test2021';
        pac2.package_number__c = 'test2021';
        pac2.From__c = 'MYVNPOST';
        
        List<Package__c> l_pac = new List<Package__c>();
        l_pac.add(pac1);
        l_pac.add(pac2);
        insert l_pac;
        
    }
}