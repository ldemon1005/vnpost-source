global class SalesOrderSchedule implements Schedulable, Database.AllowsCallouts,Database.Batchable<sObject> {
    Static final List<String> merchants = new List<String>{'MYVNPOST', 'BCCP', 'GATEWAY'};
    global void execute(SchedulableContext ctx) {
        Database.executebatch(new SalesOrderSchedule());
    }
    public Iterable<sObject> start(Database.Batchablecontext BC){
        System.debug('JOB SalesOrder');
        list<SObject> l_sobject = new List<SObject>();
        // SOBJECT        
        List<Batch__c> batchs = [SELECT Id, SalesOrder_Number__c, SalesOrder__c, from__c
                            	FROM Batch__c WHERE from__c in :merchants and (SalesOrder_Number__c != '' or SalesOrder_Number__c != null) and (SalesOrder__c = '' or SalesOrder__c = null) 
                                ORDER BY CreatedDate limit 100];
        if(batchs.size() > 0){
            for(Batch__c batch : batchs){
                l_sobject.add(batch);
            }
        	System.debug('Batch__c: '+batchs.size());
    	}
        
        List<Package__c> packages = [SELECT Id, SalesOrder_Number__c,Package_Number__c, SalesOrder__c, from__c
                            FROM Package__c WHERE  from__c in :merchants and (SalesOrder__c != '' or SalesOrder__c != null) ORDER BY CreatedDate limit 100];
        if(packages.size() > 0){
            for(Package__c pac : packages){
                l_sobject.add(pac);
            }
        	System.debug('Package__c: '+packages.size());
    	}
        
        List<Item__c> items = [SELECT Id, Package_Number__c, Package__c, from__c
                            FROM Item__c WHERE  from__c in :merchants and (Package__c = '' or Package__c = null) ORDER BY CreatedDate limit 100];
        if(items.size() > 0){
            for(Item__c item : items){
                l_sobject.add(item);
            }
        	System.debug('Item__c: '+items.size());
    	}
        
        List<Receipt__c > receipts = [SELECT Id, Package_Number__c, Package__c, from__c
                            FROM Receipt__c  WHERE  from__c in :merchants and (Package__c = '' or Package__c = null) ORDER BY CreatedDate limit 100];
        if(receipts.size() > 0){
            for(Receipt__c receipt : receipts){
                l_sobject.add(receipt);
            }
        	System.debug('Receipt__c: '+receipts.size());
    	}
        
        List<Status__c> statuss = [SELECT Id, Package_Number__c, Tracking_Code__c, from__c
                            FROM Status__c WHERE  from__c in :merchants and (Tracking_Code__c = '' or Tracking_Code__c = null) ORDER BY CreatedDate desc limit 100];
        if(statuss.size() > 0){
            for(Status__c status : statuss){
                l_sobject.add(status);
            }
        	System.debug('Status__c: '+statuss.size());
    	}
        List<Added_Value_in_Package__c> addedValues = [SELECT Id, Package_Number__c, Tracking_Code__c, from__c
                            FROM Added_Value_in_Package__c WHERE  from__c in :merchants and (Tracking_Code__c = '' or Tracking_Code__c = null) ORDER BY CreatedDate desc limit 100];
        if(addedValues.size() > 0){
            for(Added_Value_in_Package__c addedValue : addedValues){
                l_sobject.add(addedValue);
            }
        	System.debug('Added_Value_in_Package__c: '+addedValues.size());
    	}
        System.debug(l_sobject);
        return l_sobject;
    }
    
    public void execute(Database.BatchableContext BC, List<Sobject> l_sobject){
        if(l_sobject.size() > 0){
            update l_sobject;
            System.debug(l_sobject.size());
        }
    }

    public void finish(Database.BatchableContext info){
        Id job = info.getJobId();
        System.debug('SalesOrder Schedule --- Job Id: '+job);
    }
}