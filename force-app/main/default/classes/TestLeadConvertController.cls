@isTest
public class TestLeadConvertController{
	@isTest
    public static void testDoConvertLead() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
		Test.startTest();
        req.requestURI = '/services/apexrest/Lead/convert';  
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response = res;
        Lead lead = new Lead(LastName='Tên cơ hội', Status='Attempted_to_contact',Company='CMC');
        Contact contact = new Contact(LastName='Lien lac');
        Account account = new Account(Name='Cong ty');
        insert lead;
        insert contact;
        insert account;
        String JSONMsg = '{"ListLeadConvert":[{"LeadId": "'+lead.Id+'","AccountId": "'+account.Id+'","ContactId": "'+contact.Id+'","OpportunityName": "sdf","isCreateOpp": true}'
            +']}';
        req.requestBody = Blob.valueof(JSONMsg);
        LeadConvertController.doConvertLead();
        LeadConvertController.doConvertLead();
        Test.stopTest();
    }
    @isTest
    public static void testDashboardController() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/Dashboard/statistic';  
        req.httpMethod = 'GET';
        req.addParameter('q', '{"data":"23","ads":43}');
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        DashboardController.doStatistic();
        Test.stopTest();
    }
}