@istest
public class SalesOrderTriggerHandlerTest {
    @isTest static void test(){
        Batch__c batch = new Batch__c();
        batch.From__c = 'MYVNPOST';
        batch.Batch_Number__c = 'test2020';
        batch.SalesOrder_Number__c = 'test2020';
        insert batch;
        
        Package__c pac1 = new Package__c();
        pac1.package_number__c = 'test2020';
        pac1.Batch_Number__c = 'test2020';
        pac1.SalesOrder_Number__c = 'test2020';
        pac1.From__c = 'MYVNPOST';
        pac1.Order_Number__c = 'test2020';        
        insert pac1;
        
        Account acc = new Account(name = 'test2020');
        insert acc;
        
        Order o = new Order();
        o.SalesOrderNumber__c = 'test2020';
        o.AccountId = acc.id;
        o.EffectiveDate = date.today();
        o.Status = 'Draft';
        o.From__c = 'MYVNPOST';
        o.SalesOrderNumber__c = 'test2020';
        insert o;
        
        
    }
}