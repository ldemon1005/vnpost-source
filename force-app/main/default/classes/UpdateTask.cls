global class UpdateTask implements Schedulable, Database.AllowsCallouts,Database.Batchable<sObject> {
    global void execute(SchedulableContext ctx) {
        Database.executebatch(new UpdateTask());
    }
    public Iterable<sObject> start(Database.Batchablecontext BC){
        List<Task> tasks = [SELECT Id, Record_Url__c, Call_Id__c, Refer_To__c, Subject FROM Task WHERE Record_Url__c = null AND Accepted__c = 'accepted' AND CreatedDate >= LAST_N_DAYS:1];
        // Create a task for each opportunity in the list
        if(!tasks.isEmpty()){
            List<String> myCids = new List<String>();
            for(Task t : tasks){
                if(t.Call_Id__c != null){
                    myCids.add(t.Call_Id__c);
                }
            }
            if(myCids != null){
                HTTPResponse response = CalloutCallCenter.logs(myCids);
                Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                List<Object> tasksRes = (List<Object>)m.get('body');
                if(tasksRes.size() > 0){
                    for(Task t : tasks ){
                        for(Object ts : tasksRes){
                            Map<String,Object> mts = (Map<string,Object>)ts;
                            if(t.get('Refer_To__c') != null){
                                if((t.Call_Id__c == mts.get('call_id') || t.Call_Id__c == mts.get('callee_id')) && t.Refer_To__c == mts.get('refer')){
                                    t.Record_Url__c = (String) mts.get('record_url');
                                    t.CallDurationInSeconds = (Integer) mts.get('duration');
                                }
                            }else{
                                if((t.Call_Id__c == mts.get('call_id') || t.Call_Id__c == mts.get('callee_id')) && (mts.get('refer') == null || mts.get('refer') == '')){
                                    t.Record_Url__c = (String) mts.get('record_url');
                                    t.CallDurationInSeconds = (Integer) mts.get('duration');
                                }
                            }
                        }
                    }    
                }else{
                    //return tasks;
                }
            }else{
                System.debug('-- Not have callIds --');
                //return tasks;
            }
        }else{
            //return tasks;
        }
        return tasks;
    }
    
    public void execute(Database.BatchableContext BC, List<Task> tasks){
        if(tasks.size() > 0){
            update tasks;
        }
    }

    public void finish(Database.BatchableContext info){
        Id job = info.getJobId();
        System.debug('--- Job Id: '+job);
    }
}