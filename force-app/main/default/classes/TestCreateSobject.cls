@isTest
public class TestCreateSobject {
    @isTest static void testCreateHoTro(){
        List<Ho_tro__c> lst = new List<Ho_tro__c>();
        for(Integer i =0; i<100;i++){
            Ho_tro__c htc;
            if(i <99){
                htc = new Ho_tro__c(From__c = 'MYVNPOST',UUID__c='s'+i);
            } else{
                htc = new Ho_tro__c();
            }
            
            lst.add(htc);
        }
        insert lst;
    }
    @isTest static void testCreateViPham(){
        List<Vi_pham__c> lst = new List<Vi_pham__c>();
        for(Integer i =0; i<100;i++){
            Vi_pham__c htc;
            if(i <99){
                htc = new Vi_pham__c(From__c = 'MYVNPOST',UUID__c='s'+i);
            } else{
                htc = new Vi_pham__c();
            }
            
            lst.add(htc);
        }
        insert lst;
    }
     @isTest static void testCreateTienTrinhXuLy(){
        List<Tien_trinh_xu_li__c> lst = new List<Tien_trinh_xu_li__c>();
        for(Integer i =0; i<100;i++){
            Tien_trinh_xu_li__c htc;
            if(i <99){
                htc = new Tien_trinh_xu_li__c(From__c = 'MYVNPOST',UUID__c='s'+i);
            } else{
                htc = new Tien_trinh_xu_li__c();
            }
            
            lst.add(htc);
        }
        insert lst;
    }
}