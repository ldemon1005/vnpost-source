@isTest
public class ItemTriggerTest {
    @isTest static void test(){
        Package__c pac1 = new Package__c();
        pac1.package_number__c = 'test2020';
        Package__c pac2 = new Package__c();
        pac2.package_number__c = 'test2021';
        
        List<Package__c> l_pac = new List<Package__c>();
        l_pac.add(pac1);
        l_pac.add(pac2);
        insert l_pac;
        
        Item__c item = new Item__c();
        item.From__c = 'MYVNPOST';
        item.package_number__c = 'test2020';
        insert item;
        
        item.Package_Number__c = '';
        item.Package__c = l_pac[1].Id;
        update item;
    }
}