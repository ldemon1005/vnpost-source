/**
	ClassName   : SobjectLookupController
	Description : Used as Typeahead lightning component's controller.
*/
public class SobjectLookupController 
{
    /**  
        @MethodName : getIconDetails
        @Param      : Object Name 
        @Description: Fetch ICON details using describeTabs via LightningUtility helper class.
    **/
	@AuraEnabled
    public static LightningUtility.TabIconDetails getIconDetails(String objectName) 
    {
        return LightningUtility.fetchTabIconDetails(objectName); 
    }
    
    /**  
        @MethodName : getSearchedArray
        @Param      : Object Name, Search Term
        @Description: Fetch Records based on search term
    **/
    @AuraEnabled
    public static String getSearchedArray(String objName, String searchTerm) 
    { 
        List<LookupWrapper> lstArr = new List<LookupWrapper>();
        //if(String.isNotBlank(searchTerm))
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://maps.vnpost.vn/api/autocomplete?api-version=1.1&apikey=7cb80a390b89d5fefae75fc58b01d01dd04a15bb62b3b141&text='+searchTerm);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            // Deserialize the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Map<String,Object> mpParsed = (Map<String,Object>)results.get('data');
            List<Object> lstObject = (List<Object>) mpParsed.get('features');
        	//get records based the search-term
            for(Object obj : lstObject) {
                System.debug('__________r');
                Map<String,Object> mpObj = (Map<String,Object>)obj;
                Map<String,Object> reObjlat = (Map<String,Object>)mpObj.get('geometry');
                List<Object> latlong = (List<Object>)reObjlat.get('coordinates');
                //lay dia chi vnpost
                Map<String,Object> reObj = (Map<String,Object>)mpObj.get('properties');
                String labelAddress ;
                try{
                    Map<String,Object> reOAddress = (Map<String,Object>)reObj.get('addendum');
                    Map<String,Object> OAddress = (Map<String,Object>)reOAddress.get('vnpost');
                    labelAddress = String.isBlank(String.valueOf(OAddress.get('FullAdd')))?String.valueOf(reObj.get('label')):(String.valueOf(OAddress.get('FullAdd'))+', ') + String.valueOf(reObj.get('label'));
                                      
                } catch(Exception e){
                    labelAddress = String.valueOf(reObj.get('label'));
                }
                lstArr.add(new LookupWrapper(labelAddress, String.valueOf(reObj.get('smartcode')),String.valueOf(latlong.get(0)),String.valueOf(latlong.get(1)),String.valueOf(reObj.get('region'))));
               
            }  
            System.debug(results);
        }
        return JSON.serialize(lstArr);
    }
    
    public class LookupWrapper 
    {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled
        public String id {get;set;}
        @AuraEnabled
        public String lat {get;set;}
        @AuraEnabled
        public String lon {get;set;}
        @AuraEnabled
        public String region {get;set;}
        public LookupWrapper(String label, String id) 
        {
            this.label = label;
            this.id = id;
        }
        public LookupWrapper(String label, String id, String lon ,String lat) 
        {
            this.label = label;
            this.id = id;
            this.lat = lat;
            this.lon = lon;
        }
        public LookupWrapper(String label, String id, String lon ,String lat, String reg) 
        {
            this.label = label;
            this.id = id;
            this.lat = lat;
            this.lon = lon;
            this.region = reg;
        }
    }
}