public with sharing class AccountController {
        /**  
        @MethodName : getAccount
        @Param      : Account Id 
        @Description: Retrieve Account details (LastName and Account)
    **/  
    @AuraEnabled
    public static String getAccount(String conId)
    {
        // Check to make sure all fields are accessible to this user
        Set<String> fieldsToCheck = new Set<String> {
            'id', 'name'
        };
        LightningUtility.fieldAccessibleCheck(fieldsToCheck, 'Account', true, true, false);
        
        // prepare wrapper
        AccountWrap objWrap = new AccountWrap();
        if(String.isNotBlank(conId))
        {
            List<Account> lstCon = [SELECT Id, BillStreet__c, BillAddressCode__c, ShippingStreet__c,ShippingAddressCode__c
                                	FROM Account 
                                	WHERE Id =: conId];
            if(!lstCon.isEmpty())
            {
                objWrap.objCon = lstCon[0];
                /** For the reference fields using another wrapper here:
					For the null lkp/MD field-values (eg: account is a lookup field, so it can be null), 
					if we try to retrieve objAccount.Account.Name, objAccount.AccountId at ligthing comp level, it won't work. Using wrapper with 
					JSON.serializePretty with suppressApexObjectNulls = false while returning Account data helps in maintaining the binding.
				**/
				objWrap.billingAddress = new SobjectLookupController.LookupWrapper(lstCon[0].BillStreet__c, lstCon[0].BillAddressCode__c);
                objWrap.shippingAddress = new SobjectLookupController.LookupWrapper(lstCon[0].ShippingStreet__c, lstCon[0].ShippingAddressCode__c);
            }
        }
        /* If we have null values for a field, we cannot get corresponding field as an object's property in Lightning. 
         * That is why, binding becomes difficult at lightning side.
        	Even if we use wrapper class instance and return it, same issue exists on Lightning component side for null values. 
        Hence, using wrapper class + JSON.serializePretty(); with suppressApexObjectNulls = false will retain the field as properties for null values.
        Parsing this json string at the Lightning component level gives proper result.
        */
        return JSON.serializePretty(objWrap, false);  
    }
    
    /**  
        @MethodName : updateAccount
        @Param      : JSON Account wrapper
        @Description: Update Account details (LastName and Account)
    **/  
    @AuraEnabled
    public static String updateAccount(String jsonCon) 
    {
        // Check to make sure all fields are accessible to this user
        Set<String> fieldsToCheck = new Set<String> {
            'name', 'id' 
        };
        LightningUtility.fieldAccessibleCheck(fieldsToCheck, 'Account', false, false, true);
        
        String errorMsg = '';
        AccountWrap objWrap = (AccountWrap)JSON.deserialize(jsonCon, AccountWrap.class);
         
        Account objCon = new Account();
        objCon = objWrap.objCon; 
        
        // attach the account Id
        //objCon.AccountId = String.isBlank(objWrap.billingAddress.id) ? null : Id.valueOf(objWrap.billingAddress.id);
        if(String.isBlank(objWrap.billingAddress.id) != null){
            objCon.BillStreet__c = objWrap.billingAddress.label;
			objCon.BillLongitude__c = objWrap.billingAddress.lon;
            objCon.BillLatitude__c = objWrap.billingAddress.lat;
            objCon.BillAddressCode__c = objWrap.billingAddress.id;
        }
        if(String.isBlank(objWrap.shippingAddress.id) != null){
            objCon.ShippingStreet__c = objWrap.shippingAddress.label;
			objCon.ShippingLongitude__c = objWrap.shippingAddress.lon;
            objCon.ShippingLatitude__c = objWrap.shippingAddress.lat;
            objCon.ShippingAddressCode__c = objWrap.shippingAddress.id;
        }
        try 
        { 
            update objCon;
        }  
        catch(Exception ex) 
        {
            throw new AuraHandledException(ex.getMessage()); 
        }
        return objCon.Id;
    }
    
    public class AccountWrap
    {
        @AuraEnabled 
        public SobjectLookupController.LookupWrapper billingAddress {get;set;}
        @AuraEnabled 
        public SobjectLookupController.LookupWrapper shippingAddress {get;set;}
        @AuraEnabled
        public Account objCon {get;set;}
        public AccountWrap()
        {
            
        } 
    }
    
}