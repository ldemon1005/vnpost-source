public with sharing class SupportedObjectsForDMLDemoClass {
    public void sampleInsertMethod(){
        Integer count = 17;
        do {
            System.debug(count);
            count++;
        } while (count <= 20);
    }
    
    without sharing class myInnerClass {
        
    }
}