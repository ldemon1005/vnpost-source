public class BatchTriggerHandler {
    public static void updateBatchOnPackage(List<Batch__c> listBatch){
        List<String> listBatchNumber = new List<String>();
        List<Batch__c> listBatchOnPackage = new List<Batch__c>();
        for(Batch__c b: listBatch){
            if(b.Batch_Number__c!=null &&(b.From__c=='MYVNPOST' || b.From__c =='BCCP' || b.From__c =='GATEWAY')){
                listBatchNumber.add(b.Batch_Number__c);
                listBatchOnPackage.add(b);
            }
        }
        if(listBatchNumber.size()>0){
            List<Package__c> listPackage = [Select Id,Batch_Number__c From Package__c Where Batch_Number__c In :listBatchNumber AND 
                                            (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
            System.debug(listPackage);
            for(Package__c p : listPackage){
                for(Batch__c batch: listBatch){
                    if(p.Batch_Number__c == batch.Batch_Number__c){
                        p.Batch__c = batch.Id;
                    }
                }
            }
            if(listPackage.size()>0){
                update listPackage;
            }
        }
    }
    
    public static void updateOrder(List<Batch__c> listBatch){
        List<String> listUUID = new List<String>();
        List<Batch__c> listBatchUpdate = new List<Batch__c>();
        Map<String,String> mapMerchant = new Map<String,String>();
        for(Batch__c b: listBatch){
            if(b.SalesOrder_Number__c  !=null &&(b.From__c=='MYVNPOST' || b.From__c =='BCCP' || b.From__c =='GATEWAY')){
                listUUID.add(b.SalesOrder_Number__c );
                listBatchUpdate.add(b);
                mapMerchant.put(b.SalesOrder_Number__c, b.From__c);
            }
        }
        
        List<Order> l_salesOrder = [Select Id,SalesOrderNumber__c  FROM Order Where SalesOrderNumber__c  IN :listUUID 
                                      AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
        
        for(Order s: l_salesOrder){
            for(Batch__c b: listBatchUpdate){
                if(b.SalesOrder_Number__c  == s.SalesOrderNumber__c ){
                    b.Order_Number__c = s.Id;
                }
            }        
        }
    }
    
    public static void updateSaleOrderUUID(List<Batch__c> listBatch){
        List<String> listSalesOrderId = new List<String>();
        List<Batch__c> listBatchUpdate = new List<Batch__c>();
        for(Batch__c b: listBatch){
            if(b.SalesOrder_Number__c  !=null &&(b.From__c=='MYVNPOST' || b.From__c =='BCCP' || b.From__c =='GATEWAY')){
                listSalesOrderId.add(b.SalesOrder__c);
                listBatchUpdate.add(b);
            }
        }
        for(Order s: [Select Id,SalesOrderNumber__c From Order Where Id In :listSalesOrderId 
                              AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')]){
            for(Batch__c b: listBatchUpdate){
                if(b.SalesOrder__c == s.Id){
                    b.SalesOrder_Number__c  = s.SalesOrderNumber__c;
                }
            }        
        }
    }
}