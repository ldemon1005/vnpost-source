public class CalloutCallCenter {
    public static HttpResponse logs(List<String> callIds) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('http://183.91.11.56:9001/v46.0/cc/logs');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody('{"callIds":'+JSON.serializePretty(callIds)+'}');
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' +
                response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response.getBody());
        }
        return response;
    }
}