public with sharing class LeadController {
       /**  
        @MethodName : getLead
        @Param      : Lead Id 
        @Description: Retrieve Lead details (LastName and Lead)
    **/  
    @AuraEnabled
    public static String getLead(String conId)
    {
        // Check to make sure all fields are accessible to this user
        Set<String> fieldsToCheck = new Set<String> {
            'id', 'name'
        };
        LightningUtility.fieldAccessibleCheck(fieldsToCheck, 'Lead', true, true, false);
        
        // prepare wrapper
        LeadWrap objWrap = new LeadWrap();
        if(String.isNotBlank(conId))
        {
            List<Lead> lstCon = [SELECT Id, Street__c, AddressCode__c
                                	FROM Lead 
                                	WHERE Id =: conId];
            if(!lstCon.isEmpty())
            {
                objWrap.objCon = lstCon[0];
                /** For the reference fields using another wrapper here:
					For the null lkp/MD field-values (eg: Lead is a lookup field, so it can be null), 
					if we try to retrieve objLead.Lead.Name, objLead.LeadId at ligthing comp level, it won't work. Using wrapper with 
					JSON.serializePretty with suppressApexObjectNulls = false while returning Lead data helps in maintaining the binding.
				**/
				objWrap.address = new SobjectLookupController.LookupWrapper(lstCon[0].Street__c, lstCon[0].AddressCode__c);
                
            }
        }
        /* If we have null values for a field, we cannot get corresponding field as an object's property in Lightning. 
         * That is why, binding becomes difficult at lightning side.
        	Even if we use wrapper class instance and return it, same issue exists on Lightning component side for null values. 
        Hence, using wrapper class + JSON.serializePretty(); with suppressApexObjectNulls = false will retain the field as properties for null values.
        Parsing this json string at the Lightning component level gives proper result.
        */
        return JSON.serializePretty(objWrap, false);  
    }
    
    /**  
        @MethodName : updateLead
        @Param      : JSON Lead wrapper
        @Description: Update Lead details (LastName and Lead)
    **/  
    @AuraEnabled
    public static String updateLead(String jsonCon) 
    {
        // Check to make sure all fields are accessible to this user
        Set<String> fieldsToCheck = new Set<String> {
            'name', 'id' 
        };
        LightningUtility.fieldAccessibleCheck(fieldsToCheck, 'Lead', false, false, true);
        
        String errorMsg = '';
        LeadWrap objWrap = (LeadWrap)JSON.deserialize(jsonCon, LeadWrap.class);
         
        Lead objCon = new Lead();
        objCon = objWrap.objCon; 
        
        // attach the Lead Id
        //objCon.LeadId = String.isBlank(objWrap.billingAddress.id) ? null : Id.valueOf(objWrap.billingAddress.id);
        if(String.isBlank(objWrap.address.id) != null){
            objCon.Street__c = objWrap.address.label;
			objCon.Longitude__c = objWrap.address.lon;
            objCon.Latitude__c = objWrap.address.lat;
            objCon.AddressCode__c = objWrap.address.id;
        }
        try 
        { 
            update objCon;
        }  
        catch(Exception ex) 
        {
            throw new AuraHandledException(ex.getMessage()); 
        }
        return objCon.Id;
    }
    
    public class LeadWrap
    {
        @AuraEnabled 
        public SobjectLookupController.LookupWrapper address {get;set;}
        @AuraEnabled
        public Lead objCon {get;set;}
        public LeadWrap()
        {
            
        } 
    }
    
}