public class SalesOrderTriggerHandler {
    public static void updateBatch(List<Order> listSalesOrder){
        List<String> listUUID = new List<String>();
        List<Order> listSalesOrderOnBatch = new List<Order>();
        for(Order s : listSalesOrder){
            if(s.SalesOrderNumber__c !=null &&(s.From__c=='MYVNPOST' || s.From__c =='BCCP' || s.From__c =='GATEWAY')){
                listUUID.add(s.SalesOrderNumber__c);
                listSalesOrderOnBatch.add(s);
            }
        }
        if(listUUID.size() > 0){
            List<Batch__c> listBatch = [Select Id, SalesOrder_Number__c  From Batch__c Where SalesOrder_Number__c IN :listUUID
                                       AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
            for(Batch__c b : listBatch){
                for(Order s : listSalesOrderOnBatch){
                    if(b.SalesOrder_Number__c == s.SalesOrderNumber__c){
                        b.Order_Number__c = s.Id;
                    }
                }
            }
            if(listBatch.size()>0){
                update listBatch;
            }
        }  
    }
    public static void updatePackage(List<Order> listSalesOrder){
        List<String> listUUID = new List<String>();
        List<Order> listSalesOrderOnPackage = new List<Order>();
        for(Order s : listSalesOrder){
            if(s.SalesOrderNumber__c !=null &&(s.From__c=='MYVNPOST' || s.From__c =='BCCP' || s.From__c =='GATEWAY')){
                listUUID.add(s.SalesOrderNumber__c);
                listSalesOrderOnPackage.add(s);
            }
        }
        if(listUUID.size() > 0){
            List<Package__c> listPackage = [Select Id, SalesOrder_Number__c  From Package__c Where SalesOrder_Number__c IN :listUUID
                                       AND (From__c='MYVNPOST' OR From__c ='BCCP' OR From__c ='GATEWAY')];
            for(Package__c b : listPackage){
                for(Order s : listSalesOrderOnPackage){
                    if(b.SalesOrder_Number__c == s.SalesOrderNumber__c){
                        b.Order_Number__c = s.Id;
                    }
                }
            }
            if(listPackage.size()>0){
                update listPackage;
            }
        }
            
    }
}