@isTest
public class ReceiptTriggerTest {
    @isTest static void test(){
        Package__c pac1 = new Package__c();
        pac1.package_number__c = 'test2020';
        Package__c pac2 = new Package__c();
        pac2.package_number__c = 'test2021';
        
        List<Package__c> l_pac = new List<Package__c>();
        l_pac.add(pac1);
        l_pac.add(pac2);
        insert l_pac;
        
        Receipt__c rec = new Receipt__c();
        rec.From__c = 'MYVNPOST';
        rec.package_number__c = 'test2020';
        insert rec;
        
        rec.Package_Number__c = '';
        rec.Package__c = l_pac[1].Id;
        update rec;
    }
}