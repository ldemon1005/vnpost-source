public class ItemTriggerHandler {
    public static Map<String, Item__c> updatePackageLookup(List<Item__c> l_item){
        list<String> l_id = new list<String>();
       	Map<String,Item__c> mapItemNew = new Map<String,Item__c>();
        Map<String,Item__c> mapItem = new Map<String,Item__c>();
        Map<String,String> mapMerchant = new Map<String,String>();
        for(Item__c item : l_item){
            if(item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY'){
                mapItem.put(item.Package_Number__c , item);
                l_id.add(item.Package_Number__c);
                mapMerchant.put(item.package_number__c, item.From__c);
            }
        }
        list<Package__c> l_package = [select id, Package_Number__c  from Package__c where Package_Number__c  in : l_id];
        
        if(l_package.size() > 0){
            for(Package__c pa : l_package){
                Item__c item = mapItem.get(pa.Package_Number__c);
                if(item != null){
                    item.Package__c = pa.id;
                    mapItemNew.put(item.id,item);
                }
            }
        }
        return mapItemNew;
    }
    
    public static Map<String, Item__c> updatePackageUUID(List<Item__c> l_item){
        list<String> l_id = new list<String>();
       	Map<String,Item__c> mapItemNew = new Map<String,Item__c>();
        Map<String,Item__c> mapItem = new Map<String,Item__c>();
        for(Item__c item : l_item){
            if(item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY'){
                mapItem.put(item.Package__c , item);
                l_id.add(item.Package__c);
            }
        }
        list<Package__c> l_package = [select id, Package_Number__c  from Package__c where id  in : l_id];
        if(l_package.size() > 0){
            for(Package__c pa : l_package){
                Item__c item = mapItem.get(pa.id);
                if(item != null){
                    item.Package_Number__c = pa.package_number__c;
                    mapItemNew.put(item.id,item);
                }
            }
        }
        return mapItemNew;
    }
}