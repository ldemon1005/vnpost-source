@RestResource(urlMapping='/Dashboard/statistic')
global with sharing class DashboardController {
    global class Data {
        public String obj;
        public Integer total;
    }
    global  class Response {
        public Integer code;
        public String message;
        public List<Data> data;
    }
	@HttpGet
    global static Response doStatistic() {
        String q = RestContext.request.params.get('q');
        
        Response record = new Response();
        record.message = 'ok';
        record.code = 200;
        List<Data> data = new List<Data>();
        try{
            if(q != null){
                List<String> lq = q.split(',');
                if(lq != null){
                    for (Integer i = 0; i < lq.size(); i++) {
                        List<String> lqi = lq[i].split(':');
                        if(lqi != null){
                            Integer result = Database.countQuery('SELECT count() FROM '+lqi[0]+' WHERE '+ lqi[1]);
                            System.debug(result);
                            Data dataLead = new Data();
                            dataLead.obj = lqi[0];
                            dataLead.total = result;
                            data.add(dataLead);
                        }
                    }
                }
            }
        }catch(exception ex){
            // comment here
            System.debug('***NOT CONVERTED**');
            System.debug(ex);
            record.message = ex.getMessage();
            record.code = 400;
        }
        record.data = data;
        return record;
    }
}