public class TicketHoTroHandler {
    public static void updateCaseNumber(List<Ho_tro__c> listTickets){
        List<String> listFUUID = new List<String>();
        List<Ho_tro__c> listTicketUpdate = new List<Ho_tro__c>();
        for(Ho_tro__c ticket : listTickets){
            if(ticket.F_UUID__c!= Null &&ticket.Case_Number__c == NULL && (ticket.From__c == 'MYVNPOST' || ticket.From__c == 'GATEWAY')){
                listTicketUpdate.add(ticket);
                listFUUID.add(ticket.F_UUID__c);
            }
        }
        for(Case c: [Select Id, UUID__c From Case Where UUID__c IN :listFUUID
                     AND (From__c= 'MYVNPOST' OR From__c = 'GATEWAY')]){
             for(Ho_tro__c ticket: listTicketUpdate){
                 if(c.UUID__c == ticket.F_UUID__c){
                     ticket.Case_Number__c = c.Id;
                 }       
             }             
        }
    }
}