public class CustomAccountController {
    @AuraEnabled
    public static List<FinServ__FinancialAccount__c> listAccount(String accountId, String recordTypeId){
        List<FinServ__FinancialAccount__c> l_acc = [SELECT AccNameAndNum__c,Community_Financial_Account_Match__c,
        ConnectionReceivedId,ConnectionSentId,CreatedById,CreatedDate,CumulusBankDemo__Account_Number__c,
        CumulusBankDemo__Account_SubType__c,CumulusBankDemo__Account_Type__c,CumulusBankDemo__Available_Balance__c,
        CumulusBankDemo__Money_Management__c,CumulusBankDemo__My_Card__c,CumulusBankDemo__Non_Sufficient_Funds_Alert__c,
        CumulusBankDemo__Primary_Access__c,CumulusBankDemo__Primary_Goal__c,CumulusBankDemo__Record_Keeping__c,
        External_ID__c,Financial_Name_Number__c,FinServ__Address1__c,FinServ__Address2__c,FinServ__ApplicationDate__c,
        FinServ__APY__c,FinServ__AssetRebalance__c,FinServ__AvailableCredit__c,FinServ__AverageBalance__c,
        FinServ__BalanceLastStatement__c,FinServ__Balance__c,FinServ__BookedDate__c,FinServ__CashBalance__c,
        FinServ__CashLimit__c,FinServ__City__c,FinServ__CloseDate__c,FinServ__ClosureReason__c,FinServ__CollateralDesc__c,
        FinServ__Country__c,FinServ__CreatedByMe__c,FinServ__CurrentPostedBalance__c,FinServ__DailyWithdrawalLimit__c,
        FinServ__Description__c,FinServ__Discretionary__c,FinServ__DrawPeriodMonths__c,FinServ__EscrowBalance__c,
        FinServ__ExpectedCloseDate__c,FinServ__FinancialAccountChargesAndFees__c,FinServ__FinancialAccountNumber__c,
        FinServ__FinancialAccountSource__c,FinServ__FinancialAccountType__c,FinServ__HardwareSerial__c,
        FinServ__HeldAway__c,FinServ__HoldingCount__c,FinServ__Household__c,FinServ__IncomingVolume__c,
        FinServ__InsuredAmount__c,FinServ__InterestRate__c,FinServ__InvestmentObjectives__c,FinServ__JointOwner__c,
        FinServ__LastTransactionDate__c,FinServ__LastUpdated__c,FinServ__LienHolder__c,FinServ__LoanAmount__c,
        FinServ__LoanEndDate__c,FinServ__LoanTermMonths__c,FinServ__Managed__c,FinServ__MinimumBalance__c,
        FinServ__MinimumPayment__c,FinServ__ModelPortfolio__c,FinServ__NextStatementDate__c,FinServ__Nickname__c,
        FinServ__OpenDate__c,FinServ__OutgoingVolume__c,FinServ__OverdraftAllowed__c,FinServ__OverdraftLinkedAccount__c,
        FinServ__OverdraftProtection__c,FinServ__Ownership__c,FinServ__OwnerType__c,FinServ__PaperlessDelivery__c,
        FinServ__PaymentAmount__c,FinServ__PaymentDueDate__c,FinServ__PaymentFrequency__c,FinServ__PendingDeposits__c,
        FinServ__PendingWithdrawals__c,FinServ__Performance1Yr__c,FinServ__Performance3Yr__c,FinServ__PerformanceMTD__c,
        FinServ__PerformanceQTD__c,FinServ__PerformanceYTD__c,FinServ__PolicyTerm__c,FinServ__PostalCode__c,
        FinServ__Premium__c,FinServ__PrimaryOwner__c,FinServ__PrincipalBalance__c,FinServ__ProductName__c,
        FinServ__RebalanceFrequency__c,FinServ__RecordTypeName__c,FinServ__RenewalDate__c,FinServ__RepaymentPeriodMonths__c,
        FinServ__RoutingNumber__c,FinServ__ServiceProvider__c,FinServ__ServiceType__c,FinServ__SourceSystemId__c,
        FinServ__Stage__c,FinServ__StatementFrequency__c,FinServ__State__c,FinServ__Status__c,FinServ__TargetLimit__c,
        FinServ__TaxID__c,FinServ__TaxStatus__c,FinServ__TimeHorizon__c,FinServ__TotalCreditLimit__c,
        FinServ__Type__c,Funding_Amount__c,Funding_Method__c,Id,IsDeleted,IsLocked,LastActivityDate,
        LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,MayEdit,Name,OwnerId,
        Product_Type__c,Product__c,RecordTypeId,Routing_Number__c,ServiceStatusMeasure__c,SystemModstamp,
        UCIN_External_ID__c FROM FinServ__FinancialAccount__c 
        WHERE FinServ__OverdraftLinkedAccount__c = :accountId and RecordTypeId = :recordTypeId ];
        return l_acc;
    }

    @AuraEnabled
    public static String deleteAccount(String accountId){
        List<FinServ__FinancialAccount__c> l_acc = [SELECT Id FROM FinServ__FinancialAccount__c WHERE id= :accountId]; 
        if(l_acc.size() > 0){
            delete l_acc;
        }
        return 'Success';
    }
}
