@RestResource(urlMapping='/link-document/*')
global class getLinkDoucment {
    public class Params{
        public String ids;
    }
    public class Results{
        public String id;
        public String content_id;
    }

    public class File{
        public Blob version_data;
        public String file_name;
    }    
	@HttpPost
    global static void getRecord() { 
        RestRequest paramReq = RestContext.request;
        RestResponse response = Restcontext.response;
        String paramStr = paramReq.requestBody.toString();
        List<Results> l_res = new List<Results>();
        Params param = (Params)JSON.deserializeStrict(paramStr,Params.class);
        String[] ids = param.ids.split(',');
        if(ids.size() > 0){
            List<ContentDocumentLink> l_contentDocumentLink = [select id, LinkedEntityId,ContentDocumentId from ContentDocumentLink where LinkedEntityId in : ids ];
            Map<String, ContentDocumentLink> mapContentDocumentLink = new Map<String, ContentDocumentLink>();
            if(l_contentDocumentLink.size() > 0){
                list<String> l_id = new list<string>();
                for(ContentDocumentLink item :  l_contentDocumentLink){
                    mapContentDocumentLink.put(item.ContentDocumentId, item);
                    l_id.add(item.ContentDocumentId);
                }    
                if(l_id.size() > 0){
                    list<ContentVersion> l_contentVersion = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId in :l_id AND IsLatest = true];
                    
                    for(ContentVersion content : l_contentVersion){
                        if(mapContentDocumentLink.get(content.ContentDocumentId) != null){
                            Results res = new Results();
                            res.id = mapContentDocumentLink.get(content.ContentDocumentId).LinkedEntityId;
                            res.content_id = content.Id;
                            l_res.add(res);
                        }
                    }
                    RestContext.response.addHeader('Content-Type', 'application/json');
                    RestContext.response.responseBody = Blob.valueOf(JSON.serialize(l_res));
                }   
            }
        }
    }
    
    @HttpGet
    global static void getBlob() {
        RestRequest req = RestContext.request;
        String Id= RestContext.request.params.get('ContentDocId') ;
        ContentVersion a = [SELECT Title,ContentModifiedById,ContentDocumentId,FileExtension,FileType,ContentSize,ContentUrl,Id,VersionData,VersionNumber
                            FROM ContentVersion where Id=:Id and IsLatest = true];
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type','application/json');
        File fileContent = new File();
        fileContent.version_data = a.VersionData;
        fileContent.file_name = a.Title + '.' + a.FileExtension;
        res.responseBody = Blob.valueOf(JSON.serialize(fileContent));
    } 
}