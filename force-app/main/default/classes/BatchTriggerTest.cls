@isTest
public class BatchTriggerTest {
    @isTest static void test(){
        Package__c pac1 = new Package__c();
        pac1.package_number__c = 'test2020';
        pac1.Batch_Number__c = 'test2020';
        pac1.From__c = 'MYVNPOST';
        Package__c pac2 = new Package__c();
        pac2.Batch_Number__c = 'test2021';
        pac2.package_number__c = 'test2021';
        pac2.From__c = 'MYVNPOST';
        
        List<Package__c> l_pac = new List<Package__c>();
        l_pac.add(pac1);
        l_pac.add(pac2);
        insert l_pac;
        
        
        Account acc = new Account(name = 'test2020');
        insert acc;
        
        Order o = new Order();
        o.SalesOrderNumber__c = 'test2020';
        o.AccountId = acc.id;
        o.EffectiveDate = date.today();
        o.Status = 'Draft';
        o.From__c = 'MYVNPOST';
        o.SalesOrderNumber__c = 'test2020';
        insert o;
        
        Batch__c batch = new Batch__c();
        batch.From__c = 'MYVNPOST';
        batch.Batch_Number__c = 'test2020';
        batch.SalesOrder_Number__c = 'test2020';
        insert batch;
        
        batch.Order_Number__c = o.Id;
        update batch;
		
    }
}