public class ReceiptTriggerhandler {
    public static Map<String,Receipt__c> updatePackageLookup(list<Receipt__c> l_receipt){
        list<String> l_id = new list<String>();
        Map<String,Receipt__c> mapReceiptNew = new Map<String,Receipt__c>();
        Map<String,Receipt__c> mapReceipt = new Map<String,Receipt__c>();
        Map<String,String> mapMerchant = new Map<String,String>();
        for(Receipt__c item : l_receipt){
            if(item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY'){
                mapReceipt.put(item.package_number__c , item);
                l_id.add(item.package_number__c);
                mapMerchant.put(item.package_number__c, item.From__c);
            }
        }
        list<Package__c> l_package = [select id, package_number__c  from Package__c where package_number__c  in : l_id];
  
        if(l_package.size() > 0){
            for(Package__c pa : l_package){
                Receipt__c receipt = mapReceipt.get(pa.package_number__c);
                if(receipt != null){
                    receipt.Package__c = pa.id;
                    System.debug(pa.id);
                    //receipt.IsUpdated__c = true;
                    mapReceiptNew.put(receipt.id,receipt);
                }
            }
        }
        return mapReceiptNew;
    }
    
    public static Map<String,Receipt__c> updatePackageUUID(list<Receipt__c> l_receipt){
        list<String> l_id = new list<String>();
        Map<String,Receipt__c> mapReceiptNew = new Map<String,Receipt__c>();
        Map<String,Receipt__c> mapReceipt = new Map<String,Receipt__c>();
        for(Receipt__c item : l_receipt){
            if(item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY'){
                mapReceipt.put(item.Package__c , item);
                l_id.add(item.Package__c);
            }
        }
        list<Package__c> l_package = [select id, package_number__c  from Package__c where id  in : l_id];
        if(l_package.size() > 0){
            for(Package__c pa : l_package){
                Receipt__c receipt = mapReceipt.get(pa.id);
                if(receipt != null){
                    receipt.Package_Number__c = pa.package_number__c;
                    mapReceiptNew.put(receipt.id,receipt);
                }
            }
        }
        return mapReceiptNew;
    }
}