@isTest
public class SalesOrderTriggerTest {
    @isTest static void test(){
        Batch__c batch = new Batch__c(SalesOrder_Number__c='test2020', from__c = 'MYVNPOST');
        insert batch;
        
        Package__c pac = new Package__c(SalesOrder_Number__c='test2020', from__c = 'MYVNPOST');
        insert pac;
        
        SalesOrder__c order = new SalesOrder__c();
        order.SalesOrder_Number__c = 'test2020';
        order.from__c = 'MYVNPOST';
        insert order;
    }
}