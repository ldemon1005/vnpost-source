@isTest
public class AddedValueInPackageTest {
    @isTest static void test(){
        Package__c pac1 = new Package__c();
        pac1.package_number__c = 'test2020';
        Package__c pac2 = new Package__c();
        pac2.package_number__c = 'test2021';
        
        List<Package__c> l_pac = new List<Package__c>();
        l_pac.add(pac1);
        l_pac.add(pac2);
        insert l_pac;
        
        Added_Value_in_Package__c added_value = new Added_Value_in_Package__c();
        added_value.From__c = 'MYVNPOST';
        added_value.package_number__c = 'test2020';
        insert added_value;
        
        added_value.Package_Number__c = '';
        added_value.Tracking_Code__c = l_pac[1].Id;
        update added_value;
    }
}