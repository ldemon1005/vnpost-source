public class DownloadDocument {
    public class CustomDocument {
        public String name;
        public String type;
        public String url;
        public String file_date;
        public String file_time;
        public String file_desc;
        public String file_size;
    }
    
    @AuraEnabled 
    public static List<Map<String,String>> getLinksDownload(String id){
        List<Map<String,String>> listUrl = new List<Map<String,String>>();
        Opportunity opp = [select id, Download_File__c from Opportunity where id =: id];
        list<CustomDocument> oppUrls = (List<CustomDocument>)JSON.deserializeStrict(
        opp.Download_File__c,
        List<CustomDocument>.class);
        for(CustomDocument customDoc : oppUrls){
            Map<String,String> url = new Map<String,String>();
            url.put('key1',customDoc.name);
            url.put('key2',customDoc.url);
            listUrl.add(url);
        }
        System.debug(listUrl);
        return listUrl;
    }
}