global class GuidGenerator {
	private static String kHexChars = '0123456789abcdef';
    global static String generateGUID(){
  		String returnValue = '';
        Integer nextByte = 0;

        for (Integer i=0; i<16; i++) {

            if (i==4 || i==6 || i==8 || i==10) 
                returnValue += '-';

            nextByte = (Math.round(Math.random() * 255)-128) & 255;

            if (i==6) {
                nextByte = nextByte & 15;
                nextByte = nextByte | (4 << 4);
            }

            if (i==8) {
                nextByte = nextByte & 63;
                nextByte = nextByte | 128;
            }

            returnValue += charAt(kHexChars, nextByte >> 4);
            returnValue += charAt(kHexChars, nextByte & 15);
        }

        return returnValue;
    }

    public static String charAt(String str, Integer index){
       
        if (str == null) return null;

        if (str.length() <= 0) return str;    

        if (index == str.length()) return null;    

        return str.substring(index, index+1);
    }
    public static void generateUUIDSObject(List<sObject> listSObj){   
        try{
            for (sObject rec: listSObj){
                Object c = GuidGenerator.generateGUID();
                if(rec.get('UUID__c')==null){
                    rec.put('UUID__c',c); 
                }
            }
        }catch(Exception e){
            System.debug('Khong ton tai field UUID__c');
        }
    }
    public static void genSuiteCrmId(List<sObject> listSObj){   
        try{
            for (sObject rec: listSObj){
                Object c = GuidGenerator.generateGUID();
                if(rec.get('ScrmId__c')==null){
                    rec.put('ScrmId__c',c); 
                }
            }
        }catch(Exception e){
            System.debug('Not exists field: ScrmId__c');
        }
    }
}