public class CustomEmployeeController {
    @AuraEnabled 
    public static user getUserInfo(){
        if(Schema.sObjectType.Employee__c.isAccessible() && Schema.sObjectType.User.Fields.Employee_Online__c.isAccessible()){
            User u = [select id,Employee_Online__c,(select id, Name from Employee__r) from User where id =: userInfo.getUserId()];
            //System.debug(u.Employee__r);
            return u;
        }
        return null;
    }
    
    @AuraEnabled 
    public static user updateUserInfo(String employee){
        User u = [select id,Employee_Online__c from User where id =: userInfo.getUserId()];
        u.Employee_Online__c = employee;
        update u;
        return u;
    }
}