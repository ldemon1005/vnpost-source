public class StatusTriggerHandler {
    public static Map<String,Status__c> updatePackageLookup(list<Status__c> l_status){
        list<String> l_id = new list<String>();
        Map<String,Status__c> mapStatusNew = new Map<String,Status__c>();
        Map<String,Status__c> mapStatus = new Map<String,Status__c>();
        Map<String,String> mapMerchant = new Map<String,String>();
        for(Status__c item : l_status){
            if(item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY'){
                mapStatus.put(item.package_number__c , item);
                l_id.add(item.package_number__c);
                mapMerchant.put(item.package_number__c, item.From__c);
            }
        }
        list<Package__c> l_package = [select id, package_number__c  from Package__c where package_number__c  in : l_id];
        
        /*if(l_package.size() < l_id.size()){
            list<package__c> createPackage = new list<package__c>();
            for(Package__c pack : l_package){
                if(l_id.indexOf(pack.Package_Number__c) >= 0){
                    l_id.remove(l_id.indexOf(pack.Package_Number__c));
                }       
            }  
            for(String i : l_id){
                package__c pac = new Package__c(name=i, Package_Number__c=i,from__c = mapMerchant.get(i));
                createPackage.add(pac);                 
            }
            Database.UpsertResult [] srList = Database.upsert(createPackage , Package__c.fields.Package_Number__c, false);
            List<Id> ids = new List<Id>();
            for (Database.UpsertResult sr : srList) {
                ids.add(sr.getId());
            }
            createPackage = [select id,package_number__c from Package__c where id in : ids];
            l_package.addALl(createPackage);
        }*/
        
        if(l_package.size() > 0){
            for(Package__c pa : l_package){
                Status__c status = mapStatus.get(pa.package_number__c);
                if(status != null){
                    status.Tracking_Code__c = pa.id;
                    mapStatusNew.put(status.id,status);
                }
            }
        }
        return mapStatusNew;
    }
    
    public static Map<String,Status__c> updatePackageUUID(list<Status__c> l_status){
        list<String> l_id = new list<String>();
        Map<String,Status__c> mapStatusNew = new Map<String,Status__c>();
        Map<String,Status__c> mapStatus = new Map<String,Status__c>();
        for(Status__c item : l_status){
            if(item.From__c == 'MYVNPOST' || item.From__c == 'BCCP' || item.From__c == 'GATEWAY'){
                mapStatus.put(item.Tracking_Code__c , item);
                l_id.add(item.Tracking_Code__c);
            }
        }
        list<Package__c> l_package = [select id, package_number__c  from Package__c where id  in : l_id];
        if(l_package.size() > 0){
            for(Package__c pa : l_package){
                Status__c status = mapStatus.get(pa.id);
                if(status != null){
                    status.package_number__c = pa.package_number__c;
                    mapStatusNew.put(status.id,status);
                }
            }
        }
        return mapStatusNew;
    }
}