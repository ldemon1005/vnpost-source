@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://183.91.11.56:9001/v46.0/cc/logs', req.getEndpoint());
        //System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"body":[{"callee_id": "902323", "call_id": "902323", "record_url":"http://abc.com.vn", "duration":32, "refer": "0356023999"}, {"callee_id": "902323", "call_id": "902323", "record_url":"http://abc.com.vn", "duration":32}]}');
        res.setStatusCode(200);
        return res;
    }
}