({
	doInit : function(component, event, helper) {
		var actionGetUrl = component.get('c.getUrlAudio');  
        console.log(component.get("v.recordId"))
        actionGetUrl.setParams({
            "idTask": component.get("v.recordId")
        });
        actionGetUrl.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state', state);
            if (state === "SUCCESS") {
                component.set('v.audioUrl', response.getReturnValue() + '');
                console.log(component.get('v.audioUrl'))
            }
        });
        $A.enqueueAction(actionGetUrl);
	}
})