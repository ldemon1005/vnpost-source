/*
Copyright 2016 salesforce.com, inc. All rights reserved.

Use of this software is subject to the salesforce.com Developerforce Terms of Use and other applicable terms that salesforce.com may make available, as may be amended from time to time. You may not decompile, reverse engineer, disassemble, attempt to derive the source code of, decrypt, modify, or create derivative works of this software, updates thereto, or any part thereof. You may not use the software to engage in any development activity that infringes the rights of a third party, including that which interferes with, damages, or accesses in an unauthorized manner the servers, networks, or other properties or services of salesforce.com or any third party.

WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. IN NO EVENT SHALL SALESFORCE.COM HAVE ANY LIABILITY FOR ANY DAMAGES, INCLUDING BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, OR DAMAGES BASED ON LOST PROFITS, DATA OR USE, IN CONNECTION WITH THE SOFTWARE, HOWEVER CAUSED AND, WHETHER IN CONTRACT, TORT OR UNDER ANY OTHER THEORY OF LIABILITY, WHETHER OR NOT YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*/

({
    // enable click to dial, and bring up the phone panel.
    handleLogin : function(cmp) {
        var username = '';
        var password = '';
        var uri = '';
        var webSocket = '';
        try {
            var extensions = cmp.get('v.extensions');
            var extension_id = cmp.find('extension').get('v.value');
            var extension_index = extensions.findIndex(x => x.id == extension_id);
            var extension = extensions[extension_index];
            if(extension && extension.type == "webrtc"){
                username = extension.username;
                password = extension.password;
                uri = 'sip:' + username + '@' + extension.pbx.hostname;
                webSocket = extension.pbx.webrtc.protocol + "://" + extension.pbx.webrtc.host + ":" + extension.pbx.webrtc.port + extension.pbx.webrtc.path;
            
                var configuration = {
                    "register": true,
                    "displayName": `${username}`,
                    "uri": `${uri}`,
                    "authorizationUser": `${username}`,
                    "password": `${password}`,
                    "transportOptions": {"wsServers": [webSocket]},
                    "sessionDescriptionHandlerOptions": {
                        "constraints": {
                            "audio": true,
                            "video": false
                        },
                    }
                };
                var ua = new SIP.UA(configuration);
            
                window.ua = ua;
                console.log('ua 1: ', ua);
                ua.on('registered', function (e) {
                    sforce.opencti.enableClickToDial({
                        callback: function () {
                            console.log(2);
                            if (cmp.getEvent('renderPanel')) {
                                cmp.getEvent('renderPanel').setParams({
                                    type: 'c:phonePanel',
                                    attributes: {
                                        presence: 'Available'
                                    }
                                }).fire();
                            }
                        }
                    });
                });
            }else if(extension && extension.type == "sip"){
                this.socketConnect(cmp, extension);
                cmp.set('v.success', 'Login sip successfully!');
                cmp.set('v.login', true);
            }
        } catch (e) {
            cmp.set('v.error', e)
        }
    },
    socketConnect : function(cmp,extension) {
        var socketUrl = "https://socket.api-connect.io/";
        var extensionUsername = extension.username;
        var extensionPassword = extension.password;
    
    	//var extensionUsername = "9ffw7p";
        //var extensionPassword = "ie7s3e";
        var token = btoa(`${extensionUsername}:${extensionPassword}`);
        var extensionIO = io.connect(socketUrl,{
            query: {
                resource: "extension",
                token: token
            }
        });
    	window.extensionIO = extensionIO;
    	console.log('extensionIO',extensionIO);
    	var record;
        var that = this;
        if(extensionIO){
            extensionIO.on('callStatusChanged', function (call) {
                console.log("callStatusChanged:",call);	
                if(call){
                    var number = '';
                    if(call.type == 'outbound'){
                        number = call.to;
                    }
                    else if(call.type == 'inbound'){
                        number = call.from;
                    }
                    console.log('number:',number)
                    if(call.status == 'answer'){
                        cmp.set('v.success', 'Has call ' + number);
                        that.searchContact(cmp, number, function(cmp, result) {
                            record = result;
                            console.log('result:',result)
                            if(result){
                                sforce.opencti.screenPop({
                                    type : sforce.opencti.SCREENPOP_TYPE.SOBJECT,
                                    params : { recordId : result.Id },
                                    callback : function(response) {
                                        console.log('response: ',response)
                                    }
                                });
                            }
                        });
                    }
                    if(call.status == 'hangup'){
                        if(call.type == 'outbound'){
                            that.saveLog(cmp, call, record, 'Outbound', 'Call out');
                        }else if(call.type == 'inbound'){
                            that.saveLog(cmp, call, record, 'Inbound', 'Call in');
                        }
                        
                    }   
                }
            });
        }
    },
    searchContact : function(cmp, number, onCompletion) {
        var args = {
            apexClass : 'SoftphoneContactSearchController',
            methodName : 'getContacts',
            methodParams : 'name=' + number,
            callback : function(result) {
                if (result.success) {
                    var searchResults = JSON.parse(result.returnValue.runApex);
                    onCompletion && onCompletion(cmp, searchResults[0]);
                } else {
                    throw new Error(
                        'Unable to perform a search using Open CTI. Contact your admin.');
                }
            }
        };
        sforce.opencti.runApex(args);
    },
        
        saveLog: function(cmp, callLog, record, type, desc){
            var today = new Date();
            var recordId = record ? record.Id : '';
            if(recordId != ''){
                sforce.opencti.saveLog({
                    value : {
                        entityApiName : 'Task',
                        WhoId : recordId,
                        CallDisposition : 'Internal',
                        CallObject : desc,
                        Description : desc,
                        Subject : desc + ' Log',
                        Priority : 'Normal',
                        Status : 'Completed',
                        CallType : type,
                        Type : 'Call',
                        WhatId : null,
                        Call_Id__c: callLog.calleeId,
                        TaskSubtype: 'Call',
                        ActivityDate: today,
                        Accepted__c: 'accepted',
                        Record_Url__c: callLog.recordUrl,
                        CallDurationInSeconds: callLog.answeredDuration
                    },
                    callback : function(response) {
                        console.log('response', response)
                    }
                });
            }
                
    },
})