({
	doInit : function(component, event, helper) {
		var actionGetCase = component.get('c.getCase');  
        console.log(component.get("v.recordId"))
        actionGetCase.setParams({
            "idCase": component.get("v.recordId")
        });
        actionGetCase.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.caseItem', response.getReturnValue());
                var tickets = response.getReturnValue().Ticket_h_tr__r;
                if(tickets){
                    tickets.forEach(x => {
                        switch(x.RecordType.Name)  {
                            case 'Yêu cầu thu gom' :	
                        		x.fields = [
                                	'Package__c', 'Ngay_gio_thu_gom__c'
                                ];
                            	break;
                        	case 'Thay đổi tên/ địa chỉ gom hàng' :	
                        		x.fields = [
                                    'Package__c','New_Pickup_Name__c',
                                    'New_Pickup_Country_Code__c', 'New_Pickup_District_Code__c',
                                    'New_Pickup_Street__c', 'New_Pickup_Province_Code__c',
                                    'New_Pickup_Commune_Code__c', 'New_Pickup_Zipcode__c',
                                ];
                            	break;
                        	case 'Thêm/ thay đổi số điện thoại người giao hàng' :	
                        		x.fields = [
                                	'Package__c', 'New_Pickup_Phone__c'
                                ];
                            	break;
                        	case 'Thay đổi thời gian thu gom' :	
                        		x.fields = [
                                	'Package__c', 'DateTime_Change__c'
                                ];
                            	break;
                        	case 'Thêm/ thay đổi số điện thoại người gửi' :	
                        		x.fields = [
                                	'Package__c', 'New_Sender_Phone__c'
                                ];
                            	break;
                        	case 'Thêm/ thay đổi số điện thoại người nhận' :	
                        		x.fields = [
                                	'Package__c', 'New_Receiver_Phone__c'
                                ];
                            	break;
                        	case 'Lưu giữ bưu gửi đến ngày' :	
                        		x.fields = [
                                	'Package__c', 'DateTime_Required__c'
                                ];
                            	break;
                        	case 'Tách đơn, phát 1 phần bưu gửi' :	
                        		x.fields = [
                                	'Package__c', 'So_tien_phai_thu__c', 'Ten_loai_hang_duoc_phat__c'
                                ];
                            	break;
                        	case 'Thu thêm cước ở người nhận' :	
                        		x.fields = [
                                	'Package__c', 'Tien_cuoc_Phi_huy_don_hang__c', 'So_tien__c'
                                ];
                            	break;
                        	case 'Thay đổi họ tên, địa chỉ người gửi' :	
                        		x.fields = [
                                    'New_Sender_Province_Code__c', 'New_Sender_Country_Code__c', 
                                    'New_Sender_Commune_Code__c', 'New_Sender_Name__c',
                                    'New_Sender_District_Code__c', 'Package__c', 
                                    'New_Sender_Street__c', 'New_Sender_Zipcode__c'
                                ];
                            	break;
                        	case 'Thay đổi họ tên, địa chỉ người nhận' :	
                        		x.fields = [
                                    'Package__c', 'New_Receiver_Province_Code__c', 
                                    'New_Receiver_Commune_Code__c', 'New_Receiver_Country_Code__c', 
                                    'New_Receiver_Name__c', 'New_Receiver_District_Code__c', 
                                    'New_Receiver_Street__c','New_Receiver_Zipcode__c'
                                ];
                            	break;
                        	case 'Điều chỉnh số tiền COD' :	
                        		x.fields = [
                                	'Package__c', 'So_tien_sau_dieu_chinh__c'
                                ];
                            	break;
                        	case 'Hủy thu gom':
                        	case 'Điện thoại trước khi phát':
                        	case 'Yêu cầu phát tại địa chỉ' :
                            case 'Yêu cầu phát lại':
                            case 'Yêu cầu liên hệ người nhận':
                            case 'Yêu cầu liên hệ người gửi': 
                            case 'Cập nhật lý do phát không thành công':
                            case 'Yêu cầu nộp tiền COD gấp':
                            case 'Yêu cầu trả tiền COD':
                            case 'Yêu cầu chuyển hoàn':	
                        		console.log(111,x.RecordType.Name);
                        		x.fields = [
                                	'Package__c'
                                ];
                            	break;
                        	default: 
                                var fields = [
                                    'Package__c'
                                ];
                                component.set("v.fieldsCreate", fields);
                        	
                        }
                    });  
                }
            }
        });
        console.log()
        $A.enqueueAction(actionGetCase);
	},
    createTicket: function (component, event, helper){
        event.preventDefault();
        const fields = event.getParam('fields');
        fields.Case_Number__c = component.get("v.recordId");
        
        
        var actionGetTicketRecordType= component.get('c.getTicketRecordType');  
        actionGetTicketRecordType.setParams({
            "idCase": component.get("v.recordId")
        });
        actionGetTicketRecordType.setCallback(this, function(response) {
            var toastEvent = $A.get("e.force:showToast");
            
            var state = response.getState();
            if (state === "SUCCESS") {
                fields.RecordTypeId = response.getReturnValue().Id;
                component.find('createTicket').submit(fields);
                toastEvent.setParams({
                    "type": "success",
                    "title": "Success!",
                    "message": "Update successfully."
                });
                setTimeout(x=>{
                	location.reload();    
                },1000);
            }else {
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error!",
                    "message": "Update error."
                });
            }
            toastEvent.fire();
            
        });
        $A.enqueueAction(actionGetTicketRecordType);
        
        
    },
    handleSuccess: function (component, event, helper){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "success",
            "title": "Success!",
            "message": "Update successfully."
        });
        toastEvent.fire();
    },
    handleError: function (component, event, helper){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "error",
            "title": "Error!",
            "message": "Update error."
        });
        toastEvent.fire();
    },
    closeModal:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    deleteTicket: function (component,event,helper) {
        var index = event.getSource().get("v.name");  
        var actionDeleteTicket1= component.get('c.deleteTicket1');  
        actionDeleteTicket1.setParams({
            "ticketId": index
        });
        actionDeleteTicket1.setCallback(this, function(response) {
            var toastEvent = $A.get("e.force:showToast");
            
            var state = response.getState();
            if (state === "SUCCESS") {
                toastEvent.setParams({
                    "type": "success",
                    "title": "Success!",
                    "message": "Delete successfully."
                });
                setTimeout(x=>{
                	location.reload();    
                },1000);
            }else {
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error!",
                    "message": "Delete error."
                });
            }
            toastEvent.fire();
            
        });
        $A.enqueueAction(actionDeleteTicket1);
    },
    openmodal: function(component,event,helper) {
        var actionGetCase = component.get('c.getCase');  
        actionGetCase.setParams({
            "idCase": component.get("v.recordId")
        });
        actionGetCase.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                switch(response.getReturnValue().Noi_dung_ho_tro__c)  {
                    case 'Yêu cầu thu gom' :	
                        var fields = [
                            'Package__c', 'Ngay_gio_thu_gom__c'
                        ];
                        break;
                    case 'Thay đổi tên/ địa chỉ gom hàng' :	
                        var fields = [
                            'Package__c','New_Pickup_Name__c',
                            'New_Pickup_Country_Code__c', 'New_Pickup_District_Code__c',
                            'New_Pickup_Street__c', 'New_Pickup_Province_Code__c',
                            'New_Pickup_Commune_Code__c', 'New_Pickup_Zipcode__c',
                        ];
                        break;
                    case 'Thêm/ thay đổi số điện thoại người giao hàng' :	
                        var fields = [
                            'Package__c', 'New_Pickup_Phone__c'
                        ];
                        break;
                    case 'Thay đổi thời gian thu gom' :	
                        var fields = [
                            'Package__c', 'DateTime_Change__c'
                        ];
                        break;
                    case 'Thêm/ thay đổi số điện thoại người gửi' :	
                        var fields = [
                            'Package__c', 'New_Sender_Phone__c'
                        ];
                        break;
                    case 'Thêm/ thay đổi số điện thoại người nhận' :	
                        var fields = [
                            'Package__c', 'New_Receiver_Phone__c'
                        ];
                        break;
                    case 'Lưu giữ bưu gửi đến ngày' :	
                        var fields = [
                            'Package__c', 'DateTime_Required__c'
                        ];
                        break;
                    case 'Tách đơn, phát 1 phần bưu gửi' :	
                        var fields = [
                            'Package__c', 'So_tien_phai_thu__c', 'Ten_loai_hang_duoc_phat__c'
                        ];
                        break;
                    case 'Thu thêm cước ở người nhận' :	
                        var fields = [
                            'Package__c', 'Tien_cuoc_Phi_huy_don_hang__c', 'So_tien__c'
                        ];
                        break;
                    case 'Thay đổi họ tên, địa chỉ người gửi' :	
                        var fields = [
                            'New_Sender_Province_Code__c', 'New_Sender_Country_Code__c', 
                            'New_Sender_Commune_Code__c', 'New_Sender_Name__c',
                            'New_Sender_District_Code__c', 'Package__c', 
                            'New_Sender_Street__c', 'New_Sender_Zipcode__c'
                        ];
                        break;
                    case 'Thay đổi họ tên, địa chỉ người nhận' :	
                        var fields = [
                            'Package__c', 'New_Receiver_Province_Code__c', 
                            'New_Receiver_Commune_Code__c', 'New_Receiver_Country_Code__c', 
                            'New_Receiver_Name__c', 'New_Receiver_District_Code__c', 
                            'New_Receiver_Street__c','New_Receiver_Zipcode__c'
                        ];
                        break;
                    case 'Điều chỉnh số tiền COD' :	
                        var fields = [
                            'Package__c', 'So_tien_sau_dieu_chinh__c'
                        ];
                        break;
                    case 'Hủy thu gom':
                    case 'Điện thoại trước khi phát':
                    case 'Yêu cầu phát tại địa chỉ' :
                    case 'Yêu cầu phát lại':
                    case 'Yêu cầu liên hệ người nhận':
                    case 'Yêu cầu liên hệ người gửi': 
                    case 'Cập nhật lý do phát không thành công':
                    case 'Yêu cầu nộp tiền COD gấp':
                    case 'Yêu cầu trả tiền COD':
                    case 'Yêu cầu chuyển hoàn':	
                        var fields = [
                            'Package__c'
                        ];
                        break;
                    default: 
                        var fields = [
                            'Package__c'
                        ]; 
                }
                component.set("v.fieldsCreate", fields);
            }
        });
        $A.enqueueAction(actionGetCase);
        
        
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    }
})