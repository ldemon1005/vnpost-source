({
    doInit: function(cmp, event, helper) {
 		var actionGetUserInfo = cmp.get('c.getUserInfo');  
        actionGetUserInfo.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var user = response.getReturnValue();
                if(user && user.Employee__r){
                    user.Employee__r.forEach(x => {
                        if(x.Id == user.Employee_Online__c) x.selected = true;
                    });   
                    cmp.set('v.employees', user.Employee__r);
                    cmp.set('v.employeeSelected', user.Employee_Online__c);
                }
                
            }else {
                cmp.set('v.error', 'Vui lòng quay lại sau');
                return;
            }
        });
        $A.enqueueAction(actionGetUserInfo);
    },
    selectEmployee : function (cmp, event, helper){
        var employee = cmp.find('employee').get('v.value');
        if(employee == ''){
            cmp.set('v.error','Please, select extension befor login!');
            return;
        }else {
            var actionUpdateUserInfo = cmp.get('c.updateUserInfo');  
            actionUpdateUserInfo.setParams({
                "employee": employee
            });
            actionUpdateUserInfo.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Select employee successfully."
                    });
                    toastEvent.fire();
                }else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Select employee error."
                    });
                    toastEvent.fire();
                    return;
                }
            });
            $A.enqueueAction(actionUpdateUserInfo);
        }
    }
})