({
	doInit : function(component, event, helper) {
		helper.fetchLead(component);
	}, 
    update : function(component, event, helper) {
        helper.saveRecord(component);
    }   
})