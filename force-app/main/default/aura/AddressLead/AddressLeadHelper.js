({
    showSpinner : function(component) {
        var spinner = component.find('spinner');
		$A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner : function(component) {
        var spinner = component.find('spinner');
		$A.util.addClass(spinner, "slds-hide"); 
    },
    validateInputs: function(component)
    {
    	var LeadWrap = component.get("v.leadWrap");
        var isValid = true;
        // if Id is empty
        if(isValid && $A.util.isEmpty(LeadWrap.address.id))
        {
            isValid = false;
            /* if Id is empty but label is not empty => user has typed something in search box 
             * but not selected any value or not closed the searchbox, throw error */
            if(!$A.util.isEmpty(LeadWrap.address.label))
            {
                this.fireErrorEvent("address", "Please select value from lookup menu");
            }
            /* if Id is empty & label is empty => required field validation */
            else
            {
                this.fireErrorEvent("address", "You must enter a value");
            }
        } 
        return(isValid);
	},
    fireErrorEvent : function(aljsId, errorMsg)
    {
    	var errorEvt = $A.get("e.c:ALJSErrorEvent");
        errorEvt.setParams({
            "errorMsg" : errorMsg, 
            "aljsId" : aljsId
    	});     
    	errorEvt.fire(); 
	},
    saveRecord : function(component) {
		if(this.validateInputs(component))
        {
            this.showSpinner(component);
            var action = component.get("c.updateLead");
			var LeadWrap = component.get("v.leadWrap");
			
            action.setParams({
                "jsonCon" : JSON.stringify(LeadWrap), 
            });
			
            action.setCallback(this, function(response) {
                 //store state of response
                 var state = response.getState();
                    
                 if (state === "SUCCESS") { 
                    component.set("v.successMsg","Lead updated successfully");
                    // close the Modal 
                    window.setTimeout(
                        $A.getCallback(function() { 
                            if (component.isValid()) {
                                 var navEvt = $A.get("e.force:navigateToSObject");
                                 if(navEvt != undefined)
                                 {
                                     navEvt.setParams({
                                     "recordId": response.getReturnValue(),
                                     "slideDevName": "related"
                                     });
                                     navEvt.fire(); 
                                 } 
                            }
                        }), 1000
                    );
                }
              else 
              {
                  component.set("v.errorMsgs", response.getError()[0].message);
              }
              this.hideSpinner(component);
          });
           $A.enqueueAction(action);
        }
	},
    
    fetchLead : function(component)
    {
        this.showSpinner(component);
    	var action = component.get("c.getLead");
        var conId = component.get("v.recordId");
        console.log('==conId=',conId);
        action.setParams({
            "conId" : conId
        });
        action.setCallback(this, function(response) {
             //store state of response
             var state = response.getState();
                
             if (state === "SUCCESS") { 
                 var respParsed = JSON.parse(response.getReturnValue());
                 console.log('==Success==', respParsed);
				 component.set("v.leadWrap", respParsed); 	
                 component.set("v.afterLoad", true);  
         	}
          else 
          {
              component.set("v.errorMsgs", response.getError()[0].message);
          }
          this.hideSpinner(component);
      });
        $A.enqueueAction(action); 
	},
    
})