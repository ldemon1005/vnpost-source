({
	createLead : function (component, event, helper) {
    component.find("recordLoader").saveRecord($A.getCallback(function(saveResult) {
            
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
            var company = component.get("v.caseRecord")["SuppliedCompany"];
            if(!company) {
                company = "Chưa xác định";    
            }
            var createRecordEvent = $A.get("e.force:createRecord");
            createRecordEvent.setParams({
                "entityApiName": "Lead",
                "defaultFieldValues": {            
                    "LeadSource" : "Bộ phận CSKH",  
                    "Status": "Xác định nhu cầu", 
                    "Description" : component.get("v.caseRecord")["Description"],
                    "LastName": component.get("v.caseRecord")["SuppliedName"],
                    "Phone": component.get("v.caseRecord")["SuppliedPhone"],
                    "Email": component.get("v.caseRecord")["SuppliedEmail"],
                    "Company": company,
                    "Longitude__c": component.get("v.caseRecord")["CaseNumber"], // use to create task form case
                    "Latitude__c": component.get("v.recordId"), // use to create task form case 
                },
            });
            createRecordEvent.fire();  
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
    }));
    
  },
  handleRecordUpdated: function(component, event, helper) {
  switch(event.getParams().changeType) {
    case "ERROR":
      // handle error
      break;
    case "LOADED":
      console.log("Loaded Case Data");
      console.log(component.get("v.caseRecord")["Description"]);
      break;
    case "REMOVED":
      // stuff
      break;
    case "CHANGED":
      var changedFields = event.getParams().changedFields;
      var file
      console.log('Fields that are changed: ' + JSON.stringify(changedFields));
      
      component.find("recordLoader").reloadRecord();
      break;
    }
  }
   
    
})