({
	doInit : function(component, event, helper) {
		var actionGetCase = component.get('c.getCase');  
        console.log(component.get("v.recordId"))
        actionGetCase.setParams({
            "idCase": component.get("v.recordId")
        });
        actionGetCase.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state', state);
            if (state === "SUCCESS") {
                component.set('v.caseItem', response.getReturnValue());
            }
        });
        $A.enqueueAction(actionGetCase);
	},
    
    refresh : function(component, event, helper) {
        $A.get("e.force:refreshView").fire();
    }
})