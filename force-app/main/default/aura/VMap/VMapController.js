({
    init: function (cmp, event, helper) {
        var data =cmp.get("v.mapMarkers");
       
        cmp.set('v.mapMarkers', data);

        cmp.set('v.center', {
            location: {
                City: "Ho Chi Minh"
            }
        });

        cmp.set('v.zoomLevel', 12);
        cmp.set('v.markersTitle', 'Địa chỉ');
        cmp.set('v.showFooter', true);
    }
});