({
    showSpinner : function(component) {
        var spinner = component.find('spinner');
		$A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner : function(component) {
        var spinner = component.find('spinner');
		$A.util.addClass(spinner, "slds-hide"); 
    },
    validateInputs: function(component)
    {
    	var accountWrap = component.get("v.accountWrap");
        var isValid = true;
        // if Id is empty
        if(isValid && $A.util.isEmpty(accountWrap.billingAddress.id))
        {
            isValid = false;
            /* if Id is empty but label is not empty => user has typed something in search box 
             * but not selected any value or not closed the searchbox, throw error */
            if(!$A.util.isEmpty(accountWrap.billingAddress.label))
            {
                this.fireErrorEvent("billingAddress", "Please select value from lookup menu");
            }
            /* if Id is empty & label is empty => required field validation */
            else
            {
                this.fireErrorEvent("billingAddress", "You must enter a value");
            }
        } 
        return(isValid);
	},
    fireErrorEvent : function(aljsId, errorMsg)
    {
    	var errorEvt = $A.get("e.c:ALJSErrorEvent");
        errorEvt.setParams({
            "errorMsg" : errorMsg, 
            "aljsId" : aljsId
    	});     
    	errorEvt.fire(); 
	},
    saveRecord : function(component) {
		if(this.validateInputs(component))
        {
            this.showSpinner(component);
            var action = component.get("c.updateAccount");
			var accountWrap = component.get("v.accountWrap");
			
            action.setParams({
                "jsonCon" : JSON.stringify(accountWrap), 
            });
			
            action.setCallback(this, function(response) {
                 //store state of response
                 var state = response.getState();
                    
                 if (state === "SUCCESS") { 
                    component.set("v.successMsg","Account updated successfully");
                    // close the Modal 
                    window.setTimeout(
                        $A.getCallback(function() { 
                            if (component.isValid()) {
                                 var navEvt = $A.get("e.force:navigateToSObject");
                                 if(navEvt != undefined)
                                 {
                                     navEvt.setParams({
                                     "recordId": response.getReturnValue(),
                                     "slideDevName": "related"
                                     });
                                     navEvt.fire(); 
                                 } 
                            }
                        }), 1000
                    );
                }
              else 
              {
                  component.set("v.errorMsgs", response.getError()[0].message);
              }
              this.hideSpinner(component);
          });
           $A.enqueueAction(action);
        }
	},
    
    fetchAccount : function(component)
    {
        this.showSpinner(component);
    	var action = component.get("c.getAccount");
        var conId = component.get("v.recordId");
        console.log('==conId=',conId);
        action.setParams({
            "conId" : conId
        });
        action.setCallback(this, function(response) {
             //store state of response
             var state = response.getState();
                
             if (state === "SUCCESS") { 
                 var respParsed = JSON.parse(response.getReturnValue());
                 console.log('==Success==', respParsed);
				 component.set("v.accountWrap", respParsed); 
                 component.set("v.afterLoad", true);  
                 var billAdd= respParsed["objCon"]["BillStreet__c"];
                 var arrayA = billAdd.split(",");
                 var regionBIll = "Hà Nội";
                 if(arrayA.length > 0){
                     regionBIll = arrayA[arrayA.length-1];
                 }
                 
                 var shipAdd= respParsed["objCon"]["ShippingStreet__c"];
                 var arrayS = shipAdd.split(",");
                 var regionShip = "Hà Nội";
                 if(arrayA.length > 0){
                     regionShip = arrayS[arrayS.length-1];
                 }
                 var locations=  [
                     	{
                         location: {
                                Street: billAdd,
                                City : regionBIll
                            },
            
                            icon: 'custom:custom26',
                            title: 'Billing Address',
                            description: billAdd
                        },
                        {
                            location: {
                                Street: shipAdd,
                                City : regionShip
                            },
            
                             icon: 'custom:custom96',
                            title: 'Shipping Address',
                            description: shipAdd
                        }
                   ];
                 component.set("v.mapMarkers", locations); 
         	}
          else 
          {
              component.set("v.errorMsgs", response.getError()[0].message);
          }
          this.hideSpinner(component);
      });
        $A.enqueueAction(action); 
	},
    
})