/*
Copyright 2016 salesforce.com, inc. All rights reserved.

Use of this software is subject to the salesforce.com Developerforce Terms of Use and other applicable terms that salesforce.com may make available, as may be amended from time to time. You may not decompile, reverse engineer, disassemble, attempt to derive the source code of, decrypt, modify, or create derivative works of this software, updates thereto, or any part thereof. You may not use the software to engage in any development activity that infringes the rights of a third party, including that which interferes with, damages, or accesses in an unauthorized manner the servers, networks, or other properties or services of salesforce.com or any third party.

WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. IN NO EVENT SHALL SALESFORCE.COM HAVE ANY LIABILITY FOR ANY DAMAGES, INCLUDING BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, OR DAMAGES BASED ON LOST PROFITS, DATA OR USE, IN CONNECTION WITH THE SOFTWARE, HOWEVER CAUSED AND, WHETHER IN CONTRACT, TORT OR UNDER ANY OTHER THEORY OF LIABILITY, WHETHER OR NOT YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*/

({
    deleteAccount: function(component,row){
        var actionAccountDel= component.get('c.deleteAccount');  
        actionAccountDel.setParams({
            "accountId": row.Id
        });
        actionAccountDel.setCallback(this, function(response) { 
            var toastEvent = $A.get("e.force:showToast");
            var state = response.getState();
            if (state === "SUCCESS") {
                var rows = component.get('v.financialAccounts');
                var rowIndex = -1;
                rows.some(function(current, i) {
                    if (current.Id === row.Id) {
                        rowIndex = i;
                        return true;
                    }
                });
                rows.splice(rowIndex, 1);
                component.set("v.financialAccounts", rows)

                toastEvent.setParams({
                    "type": "success",
                    "title": "Success!",
                    "message": "Delete successfully."
                });
            }else {
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error!",
                    "message": "Update error."
                });
            }
            toastEvent.fire();
        });
        $A.enqueueAction(actionAccountDel);
    },
    detailAccount: function(component,row){
        component.set("v.recorDetailId", row.Id);
        var cmpTarget = component.find('ModalboxEdit');
        var cmpBack = component.find('ModalbackdropEdit');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
})