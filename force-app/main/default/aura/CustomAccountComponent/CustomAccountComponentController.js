({
	doInit : function(component, event, helper) {
        console.log("11",typeof component.get("v.fieldsString"));
        var columns = JSON.parse(component.get("v.fieldsString"));
        console.log("22", columns);
        var actions = [
            { label: 'Show details', name: 'show_details' },
            { label: 'Delete', name: 'delete' }
        ];
        columns.push({ type: 'action', typeAttributes: { rowActions: actions } });
        component.set('v.columns', columns);

        var fieldsCreate = [];
        var fl = component.get("v.fieldsCreateString").split(",");
        fl.forEach(function(f) {
            fieldsCreate.push(f); 
            console.log("11");
        });
        component.set("v.fieldsCreate", fieldsCreate);
        
        var actionAccountList= component.get('c.listAccount');  
        actionAccountList.setParams({
            "accountId": component.get("v.recordId"),
            "recordTypeId": component.get("v.record_type_id") 
        });


        actionAccountList.setCallback(this, function(response) {
            var toastEvent = $A.get("e.force:showToast");
            var state = response.getState();
            console.log("111", response.getReturnValue())
            if (state === "SUCCESS") {
                component.set("v.financialAccounts", response.getReturnValue());
            }else {
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error!",
                    "message": "Update error."
                });
            }
            toastEvent.fire();
        });
        $A.enqueueAction(actionAccountList);
	},
    createAccount: function (component, event, helper){
        event.preventDefault();
        var fields = event.getParam('fields');
        fields.FinServ__OverdraftLinkedAccount__c = component.get("v.recordId");
        fields.RecordTypeId = component.get("v.record_type_id");
        component.find('createAccount').submit(fields);
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "success",
            "title": "Success!",
            "message": "Created successfully."
        });
        setTimeout(x=>{
            location.reload();    
        },1000);
        toastEvent.fire();
    },
    
    closeModal:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    
    openmodal: function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        switch (action.name) {
            case 'show_details':
                helper.detailAccount(component, row);
                break;
            case 'delete':
                helper.deleteAccount(component,row);
                break;
        }
    },
    closeModalEdit:function(component,event,helper){    
        var cmpTarget = component.find('ModalboxEdit');
        var cmpBack = component.find('ModalbackdropEdit');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    editAccount: function (component, event, helper){
        event.preventDefault();
        var fields = event.getParam('fields');
        component.find('editAccount').submit(fields);
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "success",
            "title": "Success!",
            "message": "Update successfully."
        });
        setTimeout(x=>{
            location.reload();    
        },1000);
        toastEvent.fire();
    },
})