/*
Copyright 2016 salesforce.com, inc. All rights reserved.

Use of this software is subject to the salesforce.com Developerforce Terms of Use and other applicable terms that salesforce.com may make available, as may be amended from time to time. You may not decompile, reverse engineer, disassemble, attempt to derive the source code of, decrypt, modify, or create derivative works of this software, updates thereto, or any part thereof. You may not use the software to engage in any development activity that infringes the rights of a third party, including that which interferes with, damages, or accesses in an unauthorized manner the servers, networks, or other properties or services of salesforce.com or any third party.

WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. IN NO EVENT SHALL SALESFORCE.COM HAVE ANY LIABILITY FOR ANY DAMAGES, INCLUDING BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, OR DAMAGES BASED ON LOST PROFITS, DATA OR USE, IN CONNECTION WITH THE SOFTWARE, HOWEVER CAUSED AND, WHETHER IN CONTRACT, TORT OR UNDER ANY OTHER THEORY OF LIABILITY, WHETHER OR NOT YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*/

({
    // get call center settings, to get the information about the call provider
    // then use open CTI to screen pop to the record, and runApex() to make a call
    screenPopAndCall : function(cmp) {
        var that = this;
        cmp.getEvent('getSettings').setParams({
            callback: function(settings) {
                sforce.opencti.screenPop({
                    type : sforce.opencti.SCREENPOP_TYPE.SOBJECT,
                    params : { recordId : cmp.get('v.recordId') },
                    callback : function(response) {
                        try{
                            cmp.getEvent('editPanel').setParams({
                                label : 'Open CTI Softphone: ' + cmp.get('v.')
                            }).fire();    
                        }catch(e){
                            console.log('editPanel: ',e);
                        }
                        
                    
                        var toNumber = cmp.get('v.phone');

                        var ua = window.ua;
                        var state = cmp.get('v.state');
                      
                        if(toNumber && state == 'Dialing'){
                            var session = ua.invite(toNumber, {
                               	media: {
                                    constraints: {
                                        audio: true,
                                        video: false
                                    }
                                },
                                sessionDescriptionHandlerOptions: {
                                    constraints: {
                                        audio: true,
                                        video: false
                                    },
                                }
                            });
                            window.sessionCallOut = session; 
                            session.on('trackAdded', function () {
                                //cmp.set('v.startDuration', true);
                                var pc = session.sessionDescriptionHandler.peerConnection;
                                var player = document.getElementById("audio");
                                var remoteStream = new MediaStream();
                    
                                pc.getReceivers().forEach(function (receiver) {
                                    remoteStream.addTrack(receiver.track);
                                });
                                if (typeof player.srcObject !== 'undefined') {
                                    console.log('remoteStream', remoteStream)
                                    player.srcObject = remoteStream;
                                } else if (typeof player.mozSrcObject !== 'undefined') {
                                    player.mozSrcObject = remoteStream;
                                } else if (typeof player.src !== 'undefined') {
                                    player.src = URL.createObjectURL(remoteStream);
                                } else {
                                    console.log('Error attaching stream to element.');
                                }
                                
                                //player.play();
                            });
                            session.on('accepted', function () {
                                cmp.set('v.startDuration', true);
                                try{
                                    that.saveLog(cmp, true);
                                }catch(e){
                                    console.log('save log error: ', e);
                                }
                            });
                            session.on('bye', function () {
                                console.log("BYE");
                                cmp.set('v.startDuration', false);
                                sforce.opencti.enableClickToDial({callback: function() {
                                    if(cmp.getEvent('renderPanel')){
                                        cmp.getEvent('renderPanel').setParams({
                                            type : 'c:phonePanel',
                                            attributes: { presence : 'Available'}
                                        }).fire();   
                                    }
                                }});
                            });
                            session.on('rejected', function () {
                                console.log("REJECTED");
                                cmp.set('v.startDuration', false);
                                try{
                                    that.saveLog(cmp, false);
                                }catch(e){
                                    console.log('save log error: ', e);
                                }
                                sforce.opencti.enableClickToDial({callback: function() {
                                    if(cmp.getEvent('renderPanel')){
                                        cmp.getEvent('renderPanel').setParams({
                                            type : 'c:phonePanel',
                                            attributes: { presence : 'Available'}
                                        }).fire();   
                                    }
                                }});
                            })
                            session.on('cancel', function () {
                                cmp.set('v.startDuration', false);
                                console.log("CANCEL");
                                sforce.opencti.enableClickToDial({callback: function() {
                                    if(cmp.getEvent('renderPanel')){
                                        cmp.getEvent('renderPanel').setParams({
                                            type : 'c:phonePanel',
                                            attributes: { presence : 'Available'}
                                        }).fire();   
                                    }
                                }});
                            })
                            session.on('failed', function () {
                                console.log("FAILED");
                                cmp.set('v.startDuration', false);
                                sforce.opencti.enableClickToDial({callback: function() {
                                    if(cmp.getEvent('renderPanel')){
                                        cmp.getEvent('renderPanel').setParams({
                                            type : 'c:phonePanel',
                                            attributes: { presence : 'Available'}
                                        }).fire();   
                                    }
                                }});
                            })
                        }
                        
                    }
                })
             }
        }).fire();
        
        var sessionRetrieve = window.sessionRetrieve;
        if(sessionRetrieve){
            sessionRetrieve.on('rejected',function(){
                console.log("REJECTED");
                try{
                    cmp.getEvent('renderPanel').setParams({
                        type : 'c:phonePanel',
                        toast : {'type': 'warning', 'message': 'Call was rejected.'},
                        attributes : { presence : cmp.get('v.presence') }
                    }).fire();
                }catch(e){
                    console.log(e)
                }
                
            })
            sessionRetrieve.on('cancel',function(){
                console.log("CANCEL");
                try{
                    cmp.getEvent('renderPanel').setParams({
                        type : 'c:phonePanel',
                        toast : {'type': 'warning', 'message': 'Call was cancel.'},
                        attributes : { presence : cmp.get('v.presence') }
                    }).fire();
                }catch(e){
                    console.log(e)
                }
                
            })
            sessionRetrieve.on('failed',function(){
                console.log("FAILED");
                try{
                    cmp.getEvent('renderPanel').setParams({
                        type : 'c:phonePanel',
                        toast : {'type': 'warning', 'message': 'Call was failed.'},
                        attributes : { presence : cmp.get('v.presence') }
                    }).fire();
                }catch(e){
                    console.log(e)
                }
                
            })
            sessionRetrieve.on('bye',function(){
                console.log("BYE");
                try{
                    cmp.getEvent('renderPanel').setParams({
                        type : 'c:phonePanel',
                        toast : {'type': 'warning', 'message': 'Call was bye.'},
                        attributes : { presence : cmp.get('v.presence') }
                    }).fire();
                }catch(e){
                    console.log(e)
                }
                
            })
        }
    },

    // on Accept, accept the call by bringing up the Connected Panel
    renderConnectedPanel : function(cmp){
        var recordId = cmp.get('v.recordId');
        var account = cmp.get('v.account');
        console.log('account, recordId : ',account, recordId);
        cmp.getEvent('renderPanel').setParams({
            type : 'c:connectedPanel',
            attributes : {
                showDialPad : false,
                recordId : recordId,
                callType : 'Inbound',
                account : account,
                recordName: cmp.get('v.recordName'),
                presence : cmp.get('v.presence')
            }
        }).fire();
    },
    searchContact : function(cmp, number, onCompletion) {
        var args = {
            apexClass : 'SoftphoneContactSearchController',
            methodName : 'getContacts',
            methodParams : 'name=' + number,
            callback : function(result) {
                if (result.success) {
                    var searchResults = JSON.parse(result.returnValue.runApex);
                    onCompletion && onCompletion(cmp, searchResults[0]);
                } else {
                    throw new Error(
                        'Unable to perform a search using Open CTI. Contact your admin.');
                }
            }
        };
        sforce.opencti.runApex(args);
    },
    saveLog: function(cmp,accept){
        var account = cmp.get('v.account');
        var number = cmp.get('v.phone');
        this.searchContact(cmp, number, function(cmp, result) {
            if(result){
                var recordId = result.Id;
             	var sessionCallOut = window.sessionCallOut;
                var callId = '';
                if(sessionCallOut && sessionCallOut.id && sessionCallOut.fromTag){
                    if(typeof sessionCallOut.id != 'string') sessionCallOut.id = sessionCallOut.id.toString();
                    if(typeof sessionCallOut.fromTag != 'string') sessionCallOut.fromTag = sessionCallOut.fromTag.toString();
                    var callId = sessionCallOut.id.replace(sessionCallOut.fromTag, ""); 
                    
                    if (recordId && recordId.length != 0){
                        var today = new Date();
                        sforce.opencti.saveLog({
                            value : {
                                entityApiName : 'Task',
                                WhoId : recordId,
                                CallDisposition : 'Internal',
                                CallObject : 'Call out',
                                Description : 'Call out',
                                Subject : 'Call Out Log',
                                Priority : 'Normal',
                                Status : 'Completed',
                                CallType : 'Outbound',
                                Type : 'Call',
                                WhatId : cmp.get('v.account') ? cmp.get('v.account').Id : null,
                                Call_Id__c: callId,
                                TaskSubtype: 'Call',
                                ActivityDate: today,
                                Accepted__c: accept ? 'accepted' : 'no-accepted'
                            },
                            callback : function(response) {
                                 console.log('response', response)
                            }
                        });
                    }
                }    
            }     
        });
    },
    // log a task for the call
    logDeclineCall : function(cmp) {
        var component = cmp;
        var sessionRetrieve = window.sessionRetrieve;
        console.log('sessionRetrieve',sessionRetrieve);
        console.log('cmp', cmp);
        var callId = '';
        if(sessionRetrieve && sessionRetrieve.id && sessionRetrieve.fromTag){
            if(typeof sessionRetrieve.id != 'string') sessionRetrieve.id = sessionRetrieve.id.toString();
            if(typeof sessionRetrieve.fromTag != 'string') sessionRetrieve.fromTag = sessionRetrieve.fromTag.toString();
            var callId = sessionRetrieve.id.replace(sessionRetrieve.fromTag, ""); 
            
            if (cmp.get('v.showDialPad')){
                console.log('Log Decline Call');
            } else {
                var today = new Date();
                sforce.opencti.saveLog({
                    value : {
                        entityApiName : 'Task',
                        WhoId : cmp.get('v.recordId') ? cmp.get('v.recordId') : null,
                        CallDisposition : 'Internal',
                        CallObject : 'Call In',
                        Description : 'decline call',
                        Subject : 'Call In Log Decline',
                        Priority : 'Normal',
                        Status : 'Completed',
                        CallType : cmp.get('v.callType'),
                        Type : 'Call',
                        WhatId : cmp.get('v.account') ? cmp.get('v.account').Id : null,
                        Call_Id__c: callId,
                        TaskSubtype: 'Call',
                        ActivityDate: today,
                        Accepted__c: 'no-accepted'
                    },
                    callback : function(response) {
                        console.log('response', response)
                    }
                });
            }
            try{
                if (!sessionRetrieve) {
                    return;
                } else if (sessionRetrieve.startTime) { // Connected
                    sessionRetrieve.bye();
                } else if (sessionRetrieve.reject) { // Incoming
                    sessionRetrieve.reject();
                } else if (sessionRetrieve.cancel) { // Outbound
                    sessionRetrieve.cancel();
                }
             	delete sessionRetrieve;   
            }catch(e){
                console.log(e);
            }
        }
    },
})