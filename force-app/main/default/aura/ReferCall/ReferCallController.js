({
	doInit : function(component, event, helper) {
		var getSalesOrder = component.get('c.getSalesOrder');  
        console.log(component.get("v.recordId"))
        getSalesOrder.setParams({
            "idOrder": component.get("v.recordId")
        });
        getSalesOrder.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state', state);
            if (state === "SUCCESS") {
                var order = response.getReturnValue();
                helper.ReferCall(component, order.Employee_Delivery_Phone__c, order.Contact__c);
            }
        });
        $A.enqueueAction(getSalesOrder);
	}
})