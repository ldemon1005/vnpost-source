({
	ReferCall : function(cmp,phone,contact_id) {
		var sessionRetrieve = window.sessionRetrieve;
        console.log('sessionRetrieve', sessionRetrieve);
        if(sessionRetrieve && sessionRetrieve.status === 12 && phone){
            cmp.set('v.isReceive','receive');
            try{
            	var sessionRefer = sessionRetrieve.refer(phone);
                window.sessionRefer = sessionRefer;
                this.saveLogRefer(cmp,phone,contact_id);
            }catch(e){
                console.log('refer error: ', e)
            }
            cmp.getEvent('renderPanel').setParams({
                type : 'c:phonePanel',
                toast : {'type': 'normal', 'message': 'Call was refer.'},
                attributes : { presence : cmp.get('v.presence')}
            }).fire();
        }
        if(!phone){
            cmp.set('v.errorPhone','No phone to refer');
        }
	},
    saveLogRefer : function(cmp, number,contact_id){
        var account = cmp.get('v.account');
        
        var sessionRetrieve = window.sessionRetrieve;
        var sessionRefer = window.sessionRefer;
        var callId = '';
        var today = new Date();
        
        //log call in
        if(sessionRetrieve){
            if(typeof sessionRetrieve.id != 'string') sessionRetrieve.id = sessionRetrieve.id.toString();
            if(typeof sessionRetrieve.fromTag != 'string') sessionRetrieve.fromTag = sessionRetrieve.fromTag.toString();
            var callId = sessionRetrieve.id.replace(sessionRetrieve.fromTag, ""); 
            console.log('callId',callId);
            sforce.opencti.saveLog({
                value : {
                    entityApiName : 'Task',
                    WhoId : contact_id ? contact_id : null,
                    CallDisposition : 'Internal',
                    CallObject : 'Call In',
                    Description : 'Call In Refer To ' + number,
                    Subject : 'Call In Refer Log',
                    Priority : 'Normal',
                    Status : 'Completed',
                    CallType : 'Outbound',
                    Type : 'Call',
                    WhatId : cmp.get('v.account') ? cmp.get('v.account').Id : null,
                    Call_Id__c: callId,
                    TaskSubtype: 'Call',
                    ActivityDate: today
                },
                callback : function(response) {
                    console.log('response 1', response)
                }
            });
        }    
    }
})