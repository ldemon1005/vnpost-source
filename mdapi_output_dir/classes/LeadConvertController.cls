@RestResource(urlMapping='/Lead/convert')
global with sharing class LeadConvertController{
    public LeadConvert[] ListLeadConvert;
    class LeadConvert {
        public String LeadId; 
        public String AccountId;    
        public String ContactId;  
        public String OpportunityName; 
        public String OwnerId;
        public Boolean isCreateOpp;  
    }
    global  class Response {
        public Integer code;
        public String message;
    }
	@HttpPost
    global static List<Response> doConvertLead() {
        Response record = new Response();
        record.message = 'ok';
        record.code = 200;
        try{
            RestRequest req = RestContext.request;
            Blob body = req.requestBody;
            String json = body.toString();
            LeadConvertController convert  = (LeadConvertController)System.JSON.deserialize(json, LeadConvertController.class);
            LeadStatus convertStatus = [SELECT Id, ApiName FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            Database.LeadConvertResult lcr ;
            System.debug('_________________');
            System.debug(convert);
            for(LeadConvert request : convert.ListLeadConvert){
                Database.LeadConvert lc = new Database.LeadConvert();
                lc.setLeadId(request.LeadId);
                if(request.AccountId!=null){
                    lc.setAccountId(request.AccountId);
                }
                if(request.ContactId!=null){
                    lc.setContactId(request.ContactId);
                }
                if(request.OpportunityName!=null){
                    lc.setOpportunityName(request.OpportunityName);
                }
                if(request.isCreateOpp!=null){
                    lc.setDoNotCreateOpportunity(request.isCreateOpp);
                }
                lc.setConvertedStatus(convertStatus.ApiName); 
                lcr = Database.convertLead(lc); 
            } 
        }
        catch(exception ex){
            record.message = ex.getMessage();
            record.code = 400;
        }
		List<Response> rep = new List<Response>();
        rep.add(record);
        return rep;
    }

}