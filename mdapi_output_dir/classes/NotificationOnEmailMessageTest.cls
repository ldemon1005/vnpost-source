@isTest(SeeAllData=true)
public class NotificationOnEmailMessageTest {
    @isTest()
    public static void testEmailMessage() {
        List<Case> l_case = new List<Case>();
        Case c1 = new Case();
        Case c2 = new Case();
        list<Group> queue1 = [select id from Group where name='EMAIL CSKH'];
        if(queue1.size() > 0){
            c2.OwnerId = queue1[0].Id;
            l_case.add(c2);
        }
        l_case.add(c1);
        insert l_case;
        list<EmailMessage> emailMessages = new list<EmailMessage>();
        for(Case c : l_case){
            EmailMessage emailMessage = new EmailMessage();
            emailMessage.ParentId = c.Id; // Parent Id
            emailMessage.Status = '3'; 
            emailMessage.Incoming = false; 
            emailMessage.relatedToId = c.Id; // related to record e.g. an opportunity
            emailMessage.fromAddress = 'sender@example.com'; // from address
            emailMessage.fromName = 'Dan Perkins'; // from name
            emailMessage.subject = 'This is the Subject!'; // email subject
            emailMessage.htmlBody = '<html><body><b>Hello</b></body></html>'; // email body
            
            // additional recipients who don’t have a corresponding contact, lead or user id in the Salesforce org (optional)
            emailMessage.toAddress = 'emailnotinsalesforce@toexample.com, anotherone@toexample.com';
            
            emailMessages.add(emailMessage); // insert
        }
        insert emailMessages;
    }
}