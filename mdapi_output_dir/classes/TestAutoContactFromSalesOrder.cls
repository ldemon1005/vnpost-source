@isTest 
public class TestAutoContactFromSalesOrder {
	@isTest 
    static void TestAutoContactFromSalesOrderForShipping() {
        // Test data setup
        Shipment__c newSalesOrder1 = new Shipment__c(Name='Test Sales_Order__c', Shipping_Email__c='newemail@newemail.com', Shipping_Phone__c='123456', Shipping_Name__c='New Contact');
        Shipment__c newSalesOrder2 = new Shipment__c(Name='Test Sales_Order__c 2', Shipping_Email__c='newemail-2@newemail.com', Shipping_Phone__c='123456', Shipping_Name__c='New Contact');
        Shipment__c newSalesOrder3 = new Shipment__c(Name='Test Sales_Order__c 3', Shipping_Email__c='newemail-3@newemail.com', Shipping_Phone__c='123456-2', Shipping_Name__c='');
        
        // Perform test
       	Test.startTest();
       	Database.SaveResult result1 = Database.insert(newSalesOrder1, false);
        Database.SaveResult result2 = Database.insert(newSalesOrder2, false);
        Database.SaveResult result3 = Database.insert(newSalesOrder3, false);
       	Test.stopTest();
    }
    
    @isTest (SeeAllData=true)
    static void TestAutoContactFromSalesOrderForSender() {
        // Test data setup
        Shipment__c newSalesOrder1 = new Shipment__c(Name='Test Sales_Order__c', Sender_Email__c='sender-newemail@newemail.com', Sender_Phone__c='0-123456', Sender_Name__c='New Contact');
        Shipment__c newSalesOrder2 = new Shipment__c(Name='Test Sales_Order__c 2', Sender_Email__c='sender-newemail-2@newemail.com', Sender_Phone__c='0-123456', Sender_Name__c='New Contact');
        Shipment__c newSalesOrder3 = new Shipment__c(Name='Test Sales_Order__c 3', Sender_Email__c='sender-newemail-3@newemail.com', Sender_Phone__c='0-123456-2', Sender_Name__c='');
        
        // Perform test
       	Test.startTest();
       	Database.SaveResult result1 = Database.insert(newSalesOrder1, false);
        Database.SaveResult result2 = Database.insert(newSalesOrder2, false);
        Database.SaveResult result3 = Database.insert(newSalesOrder3, false);
       	Test.stopTest();
        // 
    }
}