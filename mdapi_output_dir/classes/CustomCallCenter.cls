public class CustomCallCenter {
	@AuraEnabled 
    public static user getUserInfo(){
       	User u = [select id,Name,call_center_account__c,call_center_password__c from User where id =: userInfo.getUserId()];
        return u;
    }
    @AuraEnabled 
    public static String getUrlAudio(Id idTask){
       	Task tk = [select id, Record_Url__c from Task where id =: idTask];
        String url = '';
        if(tk.Record_Url__c != null && tk.Record_Url__c != ''){
            url = tk.Record_Url__c;
        }
        System.debug('url: ' + url);
        return url;
    }
    @AuraEnabled 
    public static Shipment__c getSalesOrder(Id idOrder){
       	Shipment__c order = [select id, Contact__c, Employee_Delivery_Phone__c from Shipment__c where id =: idOrder];
        return order;
    }
    
    @AuraEnabled 
    public static Contact createContact(String phone){
        Contact cont = new Contact();
        cont.Phone = phone;
        cont.LastName = phone;
        insert cont;
        return cont;
    }
    
    @AuraEnabled 
    public static String getExtension(){
        //String email = userInfo.getUserName();
        String email = 'admin2019@vnpost.com.vn.vnpostv1';
        String AppID = '5d6e38aa5213ff075f6609cf';
        String AppSecret = 'ADWJsgggvXBxetPqT89ZNPpf';
        
        String result = '';
        HTTP h = new HTTP();
        HTTPRequest r = new HTTPRequest();
        r.setEndpoint('https://api-connect.io/cloud-pbx/extension?filter={\"where\":{\"tags\":\"'+ email + '\"}}');
        Blob headerValue = Blob.valueOf(AppID + ':' + AppSecret);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        r.setHeader('Authorization', authorizationHeader);
        r.setMethod('GET');
        HTTPResponse response = h.send(r);
        
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus());
        } else {
            result = response.getBody();
        }
        System.debug(result);
        return result;
    }
}