public class CustomEmployeeController {
    @AuraEnabled 
    public static user getUserInfo(){
        User u = [select id,Employee_Online__c,(select id, Name from Employee__r) from User where id =: userInfo.getUserId()];
        System.debug(u.Employee__r);
        return u;
    }
    
    @AuraEnabled 
    public static user updateUserInfo(String employee){
        User u = [select id,Employee_Online__c from User where id =: userInfo.getUserId()];
        u.Employee_Online__c = employee;
        update u;
        return u;
    }
}