@isTest()
public class DownloadDocumentTest {
	@isTest()
    public static void test(){
        Account acc = new Account(Name = 'Test');
        insert acc;
        
		Opportunity opp = new Opportunity();
        opp.AccountId = acc.Id;
        opp.Name = 'Test';
        opp.CloseDate = Date.today();
        opp.StageName = 'Proposal';
        String document = '[{"name":"Plan_Saleforce_v1.docx","type":"docx","file_date":"2019-08-27","file_time":"05:27:50","file_size":16850,"url":"","file_desc":null},{"name":"Plan_Saleforce_v2.pdf","type":"pdf","file_date":"2019-08-27","file_time":"06:24:12","file_size":78227,"url":"","file_desc":null}]';
        opp.Download_File__c = document;
        insert opp;
        
        List<Map<String,String>> l_string = DownloadDocument.getLinksDownload(opp.Id);
    }
}