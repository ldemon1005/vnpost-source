@isTest
public class TestChatBOT {
    @isTest
    static void testGetOrderStatus(){
        Shipment__c newSalesOrder1 = new Shipment__c(Name='Test Sales_Order__c', Shipping_Email__c='newemail@newemail.com', Shipping_Phone__c='123456', Shipping_Name__c='New Contact');
      	insert newSalesOrder1;
        Status__c status1 = new Status__c(StatusText__c='trạng thái 1', Tracking_Code__c=newSalesOrder1.Id);
        Status__c status2 = new Status__c(StatusText__c='trạng thái 22', Tracking_Code__c=newSalesOrder1.Id);
        insert status1;
        insert status2;
        List<Shipment__c> listItem= [Select Name from Shipment__c limit 10];
        List<String> listSTR = new List<String>();
        listSTR.add('Test Sales_Order__c');
        ChatBOT.getOrderStatus(listSTR);
        List<String> listSTR2 = new List<String>();
        listSTR2.add('Test Sales_Order__aaaaaaaaaaaaaaac');
        ChatBOT.getOrderStatus(listSTR2);
        
    }
}