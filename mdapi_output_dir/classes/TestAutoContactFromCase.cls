@isTest
private class TestAutoContactFromCase {
    @isTest
    static void testAutoContractFromCase(){
       	
        // Test data setup
        Case case1 = new Case(SuppliedName='l1',Origin='Web',Subject='1');
        Case case2 = new Case(SuppliedName='l2',Origin='Web',Subject='2',SuppliedPhone='0354214164',SuppliedEmail ='email1@gmail.com');
        Case case3 = new Case(SuppliedName='l2',Origin='Web',Subject='3',SuppliedPhone='0354214164',SuppliedEmail ='email2@gmail.com');
        Case case4 = new Case(SuppliedName='l3',Origin='Facebook',Subject='4');
        Case case5 = new Case(SuppliedName='',Origin='Web',Subject='5');
        
        // Perform test
       	Test.startTest();
       	Database.SaveResult result1 = Database.insert(case1, false);
        Database.SaveResult result2 = Database.insert(case2, false);
        Database.SaveResult result3 = Database.insert(case3, false);
        Database.SaveResult result4 = Database.insert(case4, false);
        Database.SaveResult result5 = Database.insert(case5, false);
       	Test.stopTest();
        
        // Assert
        List<Case> cases = [SELECT ID, Subject, ContactId FROM Case];
        Case caseCreate1 = cases[0];
        Case caseCreate2 = cases[1];
        Case caseCreate3 = cases[2];
        Case caseCreate4 = cases[3];
        List<Contact> contacts = [SELECT ID, LastName, Phone, Email FROM Contact];
        System.assert(cases.size() == 5);
        System.assert(contacts.size() == 0);
    }
}