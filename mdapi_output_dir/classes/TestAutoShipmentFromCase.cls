@isTest
private class TestAutoShipmentFromCase {
    @isTest
    static void testAutoContractFromCase(){
       	
        // Test data setup
        Case case1 = new Case(Shipment_text__c='exist ship1',Origin='Web',Subject='1');
        Case case2 = new Case(Shipment_text__c='ship2',Origin='Web',Subject='1');
        Shipment__c ship1 = new Shipment__c(Name='exist ship1');
        
        
        // Perform test
       	Test.startTest();
        insert ship1;
       	Database.SaveResult result1 = Database.insert(case1, false);
        Database.SaveResult result2 = Database.insert(case2, false);
       	Test.stopTest();
        
        // Assert
        List<Case> cases = [SELECT ID, Subject, Shipment_text__c, Shipment__c FROM Case];
        List<Shipment__c> shipments = [SELECT ID, Name FROM Shipment__c];
        System.assert(cases.size() == 2);
        System.assert(shipments.size() == 2);
        Case caseCreate1 = cases[0];
        Case caseCreate2 = cases[1];
        Shipment__c ship1fromDB = shipments[0];
        Shipment__c ship2fromDB = shipments[1];
        System.assert(caseCreate1.Shipment__c == ship1fromDB.Id);
        System.assert(ship1fromDB.Name == 'exist ship1');
        System.assert(caseCreate2.Shipment__c == ship2fromDB.Id);
        System.assert(ship2fromDB.Name == 'ship2');
    }
}