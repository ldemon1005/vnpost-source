public class CustomCaseController {
    @AuraEnabled 
    public static Case getCase(Id idCase){
       	Case case_1 = [select id, Reason, Noi_dung_ho_tro__c, (select id, RecordTypeId,Name,Ngay_gio_thu_gom__c,New_Pickup_Name__c,
                                           		  New_Pickup_State__c,New_Pickup_Street__c,New_Pickup_ward__c,New_Pickup_Zipcode__c, RecordType.Name
                                           from Ticket_h_tr__r), Li_do_khieu_nai__c, Noi_dung_tu_van__c from Case where id =: idCase];
        return case_1;
    }
    
    @AuraEnabled 
    public static RecordType getTicketRecordType(Id idCase){
        Case case_1 = [select id, Reason, Noi_dung_ho_tro__c from Case where id =: idCase];
        System.debug(case_1);
       	List<RecordType> rts = [select Id,Name from RecordType where name = :case_1.Noi_dung_ho_tro__c and sobjecttype = 'Ho_tro__c'];
        if(rts.size() > 0){
        	return rts[0];    
        }else {
            return null;
        }
    }
    
    @AuraEnabled 
    public static boolean deleteTicket1(Id ticketId){
        list<Ho_tro__c> ticket = [select id from Ho_tro__c where Id = :ticketId];
        try {
            delete ticket;
        } catch (DmlException e) {
            return false;
        }
        return true;
    }
}