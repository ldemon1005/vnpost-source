public class CaseTriggerHandler {
    public static void checkDuplicatedCase(List<Case> cList) {
        // List of New Case Id
        List<String> cIdList = new List<String>();
        // List of Shipment ID
        List<String> shipmentIdList = new List<String>();
        // List of Supplied Phone
        List<String> suppliedPhoneList = new List<String>();
        // List of Case Reason (Li do khieu nai)
        List<String> caseReasonList = new List<String>();
        // List of Case Referrence
        List<String> caseRefList = new List<String>();
        
        for(Case cs : cList) {
            cIdList.add(cs.Id);
            
            if(cs.Shipment__c != null) {
                shipmentIdList.add(cs.Shipment__c);
            }
            
            if(cs.SuppliedPhone != null) {
                suppliedPhoneList.add(cs.SuppliedPhone);
            }
            
            if(cs.Li_do_khieu_nai__c != null) {
                caseReasonList.add(cs.Li_do_khieu_nai__c);
            }
            
            if(cs.Case_Reference_Code__c != null) {
                caseRefList.add(cs.Case_Reference_Code__c);
            }
        }
        
        List<Case> checkedCaseList = [SELECT Id, CaseNumber, Shipment__c, SuppliedPhone, Li_do_khieu_nai__c, Case_Reference_Code__c
                                      FROM Case
                                      WHERE (((Shipment__c IN :shipmentIdList) AND (SuppliedPhone IN :suppliedPhoneList) AND (Li_do_khieu_nai__c IN :caseReasonList)) OR Case_Reference_Code__c IN :caseRefList) AND (Id NOT IN :cIdList)];
        
        for(Case c : cList) {
            String shipment;
            String suppliedPhone;
            String reason;
            String caseRef;
            
            if(c.Shipment__c != null) {
                shipment = c.Shipment__c;
            }
            else {
                shipment = 'x';
            }
            
            if(c.SuppliedPhone != null) {
                suppliedPhone = c.SuppliedPhone;
            }
            else {
                suppliedPhone = 'x';
            }
            
            if(c.Li_do_khieu_nai__c != null) {
                reason = c.Li_do_khieu_nai__c;
            }
            else {
                reason = '12313';
            }
            
            if(c.Case_Reference_Code__c != null) {
                caseRef = c.Case_Reference_Code__c;
            }
            else {
                caseRef = 'mmm';
            }
            
            for(Case checked : checkedCaseList) {
                if((shipment.equals(checked.Shipment__c)
                    && suppliedPhone.equals(checked.SuppliedPhone) 
                    && reason.equals(checked.Li_do_khieu_nai__c)) || caseRef.equals(checked.Case_Reference_Code__c))  {
                        c.addError('This record is duplicated with Case Number: ' + checked.CaseNumber + '!');
                }
            }
        }
    }
}