/*
Copyright 2016 salesforce.com, inc. All rights reserved.

Use of this software is subject to the salesforce.com Developerforce Terms of Use and other applicable terms that salesforce.com may make available, as may be amended from time to time. You may not decompile, reverse engineer, disassemble, attempt to derive the source code of, decrypt, modify, or create derivative works of this software, updates thereto, or any part thereof. You may not use the software to engage in any development activity that infringes the rights of a third party, including that which interferes with, damages, or accesses in an unauthorized manner the servers, networks, or other properties or services of salesforce.com or any third party.

WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. IN NO EVENT SHALL SALESFORCE.COM HAVE ANY LIABILITY FOR ANY DAMAGES, INCLUDING BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, OR DAMAGES BASED ON LOST PROFITS, DATA OR USE, IN CONNECTION WITH THE SOFTWARE, HOWEVER CAUSED AND, WHETHER IN CONTRACT, TORT OR UNDER ANY OTHER THEORY OF LIABILITY, WHETHER OR NOT YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*/

({
    // enable click to dial, and bring up the phone panel.
    handleLogin : function(cmp) {
        var username = '';
        var password = '';
        var uri = '';
        var webSocket = '';
        try {
            var extensions = cmp.get('v.extensions');
            var extension_id = cmp.find('extension').get('v.value');
            var extension_index = extensions.findIndex(x => x.id == extension_id);
            var extension = extensions[extension_index];
        
            username = extension.username;
            password = extension.password;
            uri = 'sip:' + username + '@' + extension.pbx.hostname;
            webSocket = extension.pbx.webrtc.protocol + "://" + extension.pbx.webrtc.host + ":" + extension.pbx.webrtc.port + extension.pbx.webrtc.path;
        
            var configuration = {
                "register": true,
                "displayName": `${username}`,
                "uri": `${uri}`,
                "authorizationUser": `${username}`,
                "password": `${password}`,
                "transportOptions": {"wsServers": [webSocket]},
                "sessionDescriptionHandlerOptions": {
                    "constraints": {
                        "audio": true,
                        "video": false
                    },
                }
            };
            var ua = new SIP.UA(configuration);
        
        	window.ua = ua;
        	console.log('ua: ', ua);
            ua.on('registered', function (e) {
                sforce.opencti.enableClickToDial({
                    callback: function () {
                        if (cmp.getEvent('renderPanel')) {
                            cmp.getEvent('renderPanel').setParams({
                                type: 'c:phonePanel',
                                attributes: {
                                    presence: 'Available'
                                }
                            }).fire();
                        }
                    }
                });
            });
        } catch (e) {
            cmp.set('v.error', e)
        }
    }
})