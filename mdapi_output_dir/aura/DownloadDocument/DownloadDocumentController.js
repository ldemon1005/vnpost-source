({
	doInit : function(component, event, helper) {
		var actionGetUrl = component.get('c.getLinksDownload');  
        actionGetUrl.setParams({
            "id": component.get("v.recordId")
        });
        actionGetUrl.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.urls', response.getReturnValue());
                console.log(11111111,component.get('v.urls'))
            }
        });
        $A.enqueueAction(actionGetUrl);
	}
})