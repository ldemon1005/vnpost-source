/*
Copyright 2016 salesforce.com, inc. All rights reserved.

Use of this software is subject to the salesforce.com Developerforce Terms of Use and other applicable terms that salesforce.com may make available, as may be amended from time to time. You may not decompile, reverse engineer, disassemble, attempt to derive the source code of, decrypt, modify, or create derivative works of this software, updates thereto, or any part thereof. You may not use the software to engage in any development activity that infringes the rights of a third party, including that which interferes with, damages, or accesses in an unauthorized manner the servers, networks, or other properties or services of salesforce.com or any third party.

WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. IN NO EVENT SHALL SALESFORCE.COM HAVE ANY LIABILITY FOR ANY DAMAGES, INCLUDING BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES, OR DAMAGES BASED ON LOST PROFITS, DATA OR USE, IN CONNECTION WITH THE SOFTWARE, HOWEVER CAUSED AND, WHETHER IN CONTRACT, TORT OR UNDER ANY OTHER THEORY OF LIABILITY, WHETHER OR NOT YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
*/

({
    // log a task for the call
    logCall : function(cmp, callback) {
        var component = cmp;
        var sessionRetrieve = window.sessionRetrieve;
        console.log('sessionRetrieve',sessionRetrieve);
        var callId = '';
        if(sessionRetrieve.id && sessionRetrieve.fromTag){
            if(typeof sessionRetrieve.id != 'string') sessionRetrieve.id = sessionRetrieve.id.toString();
            if(typeof sessionRetrieve.fromTag != 'string') sessionRetrieve.fromTag = sessionRetrieve.fromTag.toString();
            var callId = sessionRetrieve.id.replace(sessionRetrieve.fromTag, ""); 
            console.log('sessionRetrieve:',sessionRetrieve);
            if (cmp.get('v.showDialPad')){
                callback();
            } else {
                var today = new Date();
                var str = cmp.get('v.recordId') ? '' : 'Khách vãng lai';
                var note = cmp.find('note').get('v.value');
                if(!note){
                 	note = 'Phone: ' + component.get('v.recordName');   
                }
                console.log('note: ',note)
                sforce.opencti.saveLog({
                    value : {
                        entityApiName : 'Task',
                        WhoId : cmp.get('v.recordId') ? cmp.get('v.recordId') : null,
                        CallDisposition : 'Internal',
                        CallObject : 'Call In',
                        Description : note,
                        Subject : 'Call In Log ' + str,
                        Priority : 'Normal',
                        Status : 'Completed',
                        CallType : cmp.get('v.callType'),
                        Type : 'Call',
                        WhatId : cmp.get('v.account') ? cmp.get('v.account').Id : null,
                        Call_Id__c: callId,
                        TaskSubtype: 'Call',
                        ActivityDate: today,
                        Phone: component.get('v.recordName'),
                        Accepted__c: 'accepted'
                    },
                    callback : callback
                });
            }
            try{
                if (!sessionRetrieve) {
                    return;
                } else if (sessionRetrieve.startTime) { // Connected
                    sessionRetrieve.bye();
                } else if (sessionRetrieve.reject) { // Incoming
                    sessionRetrieve.reject();
                } else if (sessionRetrieve.cancel) { // Outbound
                    sessionRetrieve.cancel();
                }
             	delete sessionRetrieve;   
            }catch(e){
                console.log(e);
            }
        }
    },
    searchContact : function(cmp, number, onCompletion) {
        var args = {
            apexClass : 'SoftphoneContactSearchController',
            methodName : 'getContacts',
            methodParams : 'name=' + number,
            callback : function(result) {
                if (result.success) {
                    var searchResults = JSON.parse(result.returnValue.runApex);
                    onCompletion && onCompletion(cmp, searchResults[0]);
                } else {
                    throw new Error(
                        'Unable to perform a search using Open CTI. Contact your admin.');
                }
            }
        };
        sforce.opencti.runApex(args);
    },
    saveLogRefer : function(cmp, number){
        var account = cmp.get('v.account');
        
        var sessionRetrieve = window.sessionRetrieve;
        var sessionRefer = window.sessionRefer;
        var callId = '';
        var recordId = cmp.get('v.recordId');
        var today = new Date();
        console.log('number',number, recordId);
        console.log('account', account);
        console.log('sessionRetrieve',sessionRetrieve);
        console.log('sessionRefer',sessionRefer);
        
        
        //log call in
        if(sessionRetrieve){
            if(typeof sessionRetrieve.id != 'string') sessionRetrieve.id = sessionRetrieve.id.toString();
            if(typeof sessionRetrieve.fromTag != 'string') sessionRetrieve.fromTag = sessionRetrieve.fromTag.toString();
            var callId = sessionRetrieve.id.replace(sessionRetrieve.fromTag, ""); 
            console.log('callId',callId);
            sforce.opencti.saveLog({
                value : {
                    entityApiName : 'Task',
                    WhoId : recordId ? recordId : null,
                    CallDisposition : 'Internal',
                    CallObject : 'Call In',
                    Description : 'Call In Refer To ' + number,
                    Subject : 'Call In Refer Log',
                    Priority : 'Normal',
                    Status : 'Completed',
                    CallType : 'Outbound',
                    Type : 'Call',
                    WhatId : cmp.get('v.account') ? cmp.get('v.account').Id : null,
                    Call_Id__c: callId,
                    TaskSubtype: 'Call',
                    ActivityDate: today,
                    Phone: number,
                    Accepted__c: 'accepted'
                },
                callback : function(response) {
                    console.log('response 1', response)
                }
            });
            
            //log call refer
            if(number != ''){
                var callReferId = sessionRefer.request.callId; 
                
                sforce.opencti.saveLog({
                    value : {
                        entityApiName : 'Task',
                        WhoId : recordId ? recordId : null,
                        CallDisposition : 'Internal',
                        CallObject : 'Call In',
                        Description : 'Call In Refer',
                        Subject : 'Call Log Refer To ' + number,
                        Priority : 'Normal',
                        Status : 'Completed',
                        CallType : 'Outbound',
                        Type : 'Call',
                        WhatId : cmp.get('v.account') ? cmp.get('v.account').Id : null,
                        Call_Id__c: callId,
                        TaskSubtype: 'Call',
                        ActivityDate: today,
                        Refer_To__c: number,
                        Phone: number,
                        Accepted__c: 'accepted'
                    },
                    callback : function(response) {
                        console.log('response 2', response)
                    }
                });        
            }
        }    
    }
})