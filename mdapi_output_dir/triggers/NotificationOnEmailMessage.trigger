trigger NotificationOnEmailMessage on EmailMessage (after insert) {
    Set<String> ids = new Set<String>();
    for(EmailMessage item : Trigger.New){
        ids.add(item.ParentId);
    }
    List<EmailMessage> emailMessage = [SELECT FromName,ParentId,RelatedToId,ReplyToEmailMessageId,Subject,Incoming FROM EmailMessage where ParentId in :ids and incoming = false];  
   
    if(ids.size() > 0 && emailMessage.size() > 0){
        list<Case> l_case = [select id, CaseNumber,OwnerId,LastModifiedById from Case where id in : ids];
        list<Group> l_group = [Select Id from Group where type='Queue' and Name='EMAIL CSKH'];
        if(l_case.size() > 0){
            for(Case caseItem : l_case){
                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                ConnectApi.EntityLinkSegmentInput entityLinkSegmentInput = new ConnectApi.EntityLinkSegmentInput();
                ConnectApi.LinkSegmentInput linkSegmentInput = new ConnectApi.LinkSegmentInput(); 
                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
                
                String sobjectType = caseItem.OwnerId.getSObjectType().getDescribe().getName();
                System.debug(sobjectType);
                if(sobjectType == 'Group'){
                    if(l_group.size() > 0){
                        String groupId = l_group[0].Id;
                        list<GroupMember> groupMember = [Select UserOrGroupId From GroupMember where GroupId =:groupId];
                        System.debug(groupMember);
                        if(groupMember.size() > 0){
                            for(GroupMember groupM : groupMember){
                                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                                mentionSegmentInput.id = groupM.UserOrGroupId;
                                messageBodyInput.messageSegments.add(mentionSegmentInput);
                            }
                        }
                    }
                    
                    textSegmentInput.text = ' has received an email to the case ';
                    messageBodyInput.messageSegments.add(textSegmentInput);
                    entityLinkSegmentInput.entityId  = caseItem.Id;
                    messageBodyInput.messageSegments.add(entityLinkSegmentInput);
                    /*linkSegmentInput.url = System.Url.getSalesforceBaseURL().toExternalForm() + '/' + caseItem.Id;
                    messageBodyInput.messageSegments.add(linkSegmentInput);*/
                    
                    feedItemInput.body = messageBodyInput;
                    feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                    feedItemInput.subjectId = caseItem.Id;
                    
                    ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
                 
                }else if(sobjectType == 'User'){
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    mentionSegmentInput.id = caseItem.OwnerId;
                    messageBodyInput.messageSegments.add(mentionSegmentInput);
                    
                    String message =  ' has received an email to the case ';
                    textSegmentInput.text = message;
                    messageBodyInput.messageSegments.add(textSegmentInput);
                    entityLinkSegmentInput.entityId  = caseItem.Id;
                    messageBodyInput.messageSegments.add(entityLinkSegmentInput);
                    /*linkSegmentInput.url = System.Url.getSalesforceBaseURL().toExternalForm() + '/' + caseItem.Id;
                    messageBodyInput.messageSegments.add(linkSegmentInput);*/
                   
                    feedItemInput.body = messageBodyInput;
                    feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                    feedItemInput.subjectId = caseItem.Id;
                    
                    ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
                }
            }
            
            update l_case;
        }
    }
}