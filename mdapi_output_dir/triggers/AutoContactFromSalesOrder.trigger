trigger AutoContactFromSalesOrder on Shipment__c (before insert) {
    for(Shipment__c s : Trigger.New) {
        // for shipping
        if (s.Shipping_Name__c != null && s.Shipping_Name__c != '') {
            List<Contact> existContacts = [SELECT Id, Name FROM Contact WHERE Phone != NULL AND Phone != '' AND Phone = :s.Shipping_Phone__c LIMIT 1];
            if (existContacts.isEmpty()) {
                existContacts = [SELECT Id, Name FROM Contact WHERE Email != NULL AND Email != '' AND Email = :s.Shipping_Email__c LIMIT 1];
            }
            if (existContacts.isEmpty()) {
                if (s.Shipping_Name__c != null && s.Shipping_Name__c != '') {
                    Contact contact = new Contact(LastName = s.Shipping_Name__c, Email = s.Shipping_Email__c, Phone = s.Shipping_Phone__c);
                    insert contact;
                    s.ShippingName__c = contact.Id; 
                }
            } else {
                Contact contact = existContacts[0];
                s.ShippingName__c = contact.Id;
            }
        }
        
        // for sender
        if (s.Sender_Name__c != null && s.Sender_Name__c != '') {
            List<Contact> existContacts = [SELECT Id, Name FROM Contact WHERE Phone != NULL AND Phone != '' AND Phone = :s.Sender_Phone__c LIMIT 1];
            if (existContacts.isEmpty()) {
                existContacts = [SELECT Id, Name FROM Contact WHERE Email != NULL AND Email != '' AND Email = :s.Sender_Email__c LIMIT 1];
            }
            if (existContacts.isEmpty()) {
                if (s.Sender_Name__c != null && s.Sender_Name__c != '') {
                    Contact contact = new Contact(LastName = s.Sender_Name__c, Email = s.Sender_Email__c, Phone = s.Sender_Phone__c);
                    insert contact;
                    s.SenderName__c = contact.Id;
                }
            } else {
                Contact contact = existContacts[0];
                s.SenderName__c = contact.Id;
            }
        }
    }
}