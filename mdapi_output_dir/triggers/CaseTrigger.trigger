trigger CaseTrigger on Case (before insert, before update, after insert, after update) {
    if(Trigger.isInsert) {
        if(Trigger.isAfter){
        	CaseTriggerHandler.checkDuplicatedCase(Trigger.new);
        }
        
        if(Trigger.isAfter){
            User u = [select id,Employee_Online__c from User where id =: userInfo.getUserId()];
       		List<Case_Employee__c> l_case_employee = new List<Case_Employee__c>();
            for(Case c : Trigger.new){
                Case_Employee__c case_employee = new Case_Employee__c();
                case_employee.Case__c = c.Id;
                case_employee.Employee__c = u.Employee_Online__c;
                case_employee.Action__c = 'insert case';
                l_case_employee.add(case_employee);
            }
            if(l_case_employee.size() > 0){
                System.debug(l_case_employee);
            	insert l_case_employee;    
            }
        }
    }
    
    if(Trigger.isUpdate) {
        System.debug('test vui');
        for(String cId : Trigger.newMap.keySet()) {
         	 if((Trigger.oldMap.get(cId).Shipment__c != Trigger.newMap.get(cId).Shipment__c)
             || (Trigger.oldMap.get(cId).SuppliedPhone != Trigger.newMap.get(cId).SuppliedPhone)
             || (Trigger.oldMap.get(cId).Li_do_khieu_nai__c != Trigger.newMap.get(cId).Li_do_khieu_nai__c)
             || (Trigger.oldMap.get(cId).Case_Reference_Code__c != Trigger.newMap.get(cId).Case_Reference_Code__c)) {
                 CaseTriggerHandler.checkDuplicatedCase(Trigger.new);
             }
        }
        if(Trigger.isAfter){
            User u = [select id,Employee_Online__c from User where id =: userInfo.getUserId()];
       		List<Case_Employee__c> l_case_employee = new List<Case_Employee__c>();
            for(Case c : Trigger.new){
                Case_Employee__c case_employee = new Case_Employee__c();
                case_employee.Case__c = c.Id;
                case_employee.Employee__c = u.Employee_Online__c;
                case_employee.Action__c = 'update case';
                l_case_employee.add(case_employee);
            }
            if(l_case_employee.size() > 0){
            	insert l_case_employee;    
            }
        }
    }
}