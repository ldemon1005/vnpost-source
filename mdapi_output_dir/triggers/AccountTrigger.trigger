trigger AccountTrigger on Account (before insert) {
    if(Trigger.isBefore){
        for(Account acc : Trigger.New){
            if(String.isEmpty(acc.ID_he_thong_cu__c)){
                acc.ID_he_thong_cu__c = acc.Id;
            }
        }
    }
}