trigger AutoShipmentFromCase on Case (before insert) {
    for(Case c : Trigger.New) {
        if (c.Shipment_text__c != '' && c.Shipment_text__c != null) {
            List<Shipment__c> existShipments = [SELECT Id, Name FROM Shipment__c WHERE Name = :c.Shipment_text__c];
        
            if (existShipments.isEmpty()) {
                Shipment__c shipment = new Shipment__c(Name = c.Shipment_text__c);
                insert shipment;
                c.Shipment__c = shipment.Id;
            } 
            else {
                Shipment__c shipment = existShipments[0];
                c.Shipment__c = shipment.Id;
            }
        }
    }
}