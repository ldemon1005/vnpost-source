<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_tgtn</fullName>
        <field>Nga_y_ti_p_nh_n__c</field>
        <formula>IF( ISPICKVAL(Status, &quot;Đã tiếp nhận&quot;)  &amp;&amp;    ISNULL(Nga_y_ti_p_nh_n__c) ,  NOW() ,     Nga_y_ti_p_nh_n__c )</formula>
        <name>Update thời gian tiếp nhận</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update thơ%CC%80i gian tiê%CC%81p nhâ%CC%A3n</fullName>
        <actions>
            <name>Update_tgtn</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Đã tiếp nhận</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
